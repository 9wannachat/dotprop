<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dotprop</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=5e447899-5ab0-45cb-a2d8-08f0ec1ec90c"> </script>
   
    <style>
      .fix_line{
      
        line-height:1.2em;
        height:3.6em;
        overflow:hidden;
      
      }
    </style>
  </head>
  <body>

<header>
    <?php
            require 'condb/condb.php';
            include 'header.php'; 
            $getPost = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                              inner join proppost on proppost.post_prop = properties.prop_id
                                              inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                              inner join operation on operation.op_id = properties.prop_oper
                                              inner join proptype on proptype.type_id = properties.prop_type ORDER BY prop_comment DESC ";
            $resPost = $conn->query($getPost);
    ?>
</header>



<main role="main">
      <?php include 'slider.php'; ?>
      <div class="text-center">
        <h1 class="th-head">โครงการยอดฮิต</h1>    
      </div>
      <div class="album py-5">
        <div class="container">

          <div class="row">
              <?php
                    while($res = $resPost->fetch_assoc()){
                      if($res['post_status'] != 002 AND $res['post_verify'] != 003){
                        $propId =  $res['prop_id'];
                    
                        $getImg = "SELECT * FROM propimage WHERE img_prop = $propId ORDER BY img_id DESC LIMIT 1";
                        $resImg = $conn->query($getImg);
                        $Img = $resImg->fetch_assoc();
                ?>
                    <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img class="card-img-top" src="upload/<?php echo $Img['img_name']; ?>" alt="Card image cap" width="200px" height="200px"> 
                        <div class="card-body">
                        <h5 class="card-title"><?php echo $res['prop_topic']; ?></h5>
                        <p class="card-text fix_line "><?php echo $res['prop_detail']; ?></p>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="text-center">
                                <a href="viewdetail.php?id=<?php echo $res['prop_id'];  ?>" class="th-head btn btn-light btn-block"><?php echo $res['op_name'];?></a>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="text-center">
                                <a href="#" class="th-head btn btn-light btn-block"><?php echo $res['prop_space'];?> ตร.ม.</a>
                            </div> 
                            </div>
                        </div>
                        </div>
                    </div>
            </div>
                <?php
                      }
                    }
                ?>    
      </div>
      <div class="text-center">
        <h1 class="th-head">บทความเท่าทันอสังหาฯ</h1>
      </div>

      <div class="album py-5">
        <div class="container">

          <div class="row">
          <?php
            $getNews = "SELECT * FROM dotprop_news";
            $resNews = $conn->query($getNews);
            while($res_News = $resNews->fetch_assoc()){
               
              ?>
               <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                  <img class="card-img-top" src="admin/newimg/<?php echo $res_News['news_img']; ?>" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title"><?php echo $res_News['news_name']; ?></h5>
                    <p class="card-text fix_line"><?php echo $res_News['news_short']; ?></p>
                    <div class="d-flex justify-content-between align-items-center">
                      <a href="viewnews.php?id=<?php echo $res_News['news_id']; ?>">อ่านต่อ >>></a>
                    </div>
                  </div>
                </div>
              </div>

              <?php
            }
          ?>
           

         
          </div>
          <div class="text-center">
            <a href="allnews.php">บทความทั้งหมด</a>
          </div>
        </div>
      </div>

  </main>

  <script>
        function validate() {
          var x= document.getElementById("pass");
          var y= document.getElementById("re-pass");
          if(x.value==y.value) return;
          else alert("password ไม่ตรงกัน");
        }
    </script>


   <!-- footer Zone -->
<?php include 'footer.html'; ?>
   <!-- end footer zone -->

  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </body>
</html>
