<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dotprop</title>

    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
    <style>

      .container-fluid {
        padding-right: 5rem;
        padding-left:  5rem;
      }
    </style>
  </head>
  <body>
    <header>
<?php include 'header.php'; ?>
</header>

<main role="main" style="min-height: 750px; ">
  <div class="container" >
    <div class="row">
      <div class="col-md-6">
        <div class="container">
            <div class="card mx-auto" style="width: 27rem; margin-top: 15%">
              <div class="card-body">
                <h4 class="card-title th-head">ยินดีต้อนรับสู่ DotpropHatyai</h4>
                <!-- Login Zone -->




                <nav>
  <div class="nav nav-tabs nav-fill sarabun" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">ลงชื่อเข้าใช้</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">สร้างบัญชีใหม่</a>
  </div>
</nav>
<div class="tab-content sarabun" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <div class="d-flex justify-content-center">
        <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" scope="public_profile,email" onlogin="checkLoginState();" auto-logout-link="true"></div>
      </div>
    <!-- <a href="" class="btn btn-primary btn-block">Log in with Facebook</a> -->
    <hr class="hr-text" data-content="หรือเชื่อมต่อด้วย">
    <form action="loginprocess.php" method="POST">
      <div class="form-group">
        <label for="email">อีเมล</label>
        <input type="text" name="email" id="email" class="form-control" placeholder="Enter email" required>
      </div>
      <div class="form-group">
        <label for="password">รหัสผ่าน</label><a href="#" class="float-right">ลืมรหัสผ่าน?</a>
        <input type="password" name="password" id="password" class="form-control" placeholder="Enter password" required>
      </div>
      <button type="submit" class="btn btn-success btn-block">เข้าสู่ระบบ</button>
    </form>
  </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <form action="register.php" method="post">
          <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
          </div>

         <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="fname">ชื่อ</label>
                <input type="text" class="form-control" id="fname"  name="fname" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="lname">สกุล</label>
                <input type="text" class="form-control" id="lname" name="lname" required>
              </div>
            </div>
         </div>

         <div class="form-group">
            <label for="tell">เบอร์โทรศัพท์</label>
            <input type="text" class="form-control" id="tell" name="tell" required>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="pass">รหัสผ่าน</label>
                <input type="password" class="form-control" id="pass" name="pass" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="re-pass">ยืนยันรหัสผ่าน</label>
                <input type="password" onblur="validate()" class="form-control" id="re-pass" >
              </div>
            </div>
         </div>

         <button type="submit" name="submit" class="btn btn-outline-success btn-block"> ยืนยันข้อมูล </button>

        </form>
  </div>
</div>


              </div>
            </div>

        </div>
      </div>
      <div class="col-md-6">
        <div class="container">
          <div class="mx-auto my-5">
            <h1 class="sarabun">เงื่อนไขรายละเอียด การฝาก ขาย ฝากเช่า</h1><hr>
            <!-- <p class="text-justify"> -->
              <ol class="sarabun">
<li class="text-justify">เลโนโวเห้งเจีย มาสด้ายะลาซ่งโมโตโรลาชุมพร แมชีนเนอรี่ไนเม็กซ์ โทมัสแพร่ ตรังเลกเชอร์ปราจีนบุรีทราวิฑ แอ๋มอิออนไงกัศมีรีอิออน โอ้ยจอห์นวิปโบตัน เพชรบุรีเปาบุ้นจิ้นไง เชียงรายยิวอำนาจเจริญ นครนายกโดราเอมอนยาเวห์ยโสธรา แพร่ เดลินิวส์ไอริชสมุทรปราการฮีบรู ทรพาพิจิตรออร์คิดโต๋โอริยา ไมยราพณ์ ไอซียูหนองบัวลำภูนครพนม โทรอาเซมขงเบ้งอ่างขาง </li>
<br>
<li class="text-justify">รีพับลิกันซ่งเบงกาลี นครปฐมโมโตโรล่ารัสปูตินเพาเวอร์ กัณณาฑพาร์ทเนอร์ ซะ ชเวดากองกินเนสกำแพงเพชรซูซานสินธี ฮ็อตพัทยาภูเก็ตซาบะ อุดรธานีอุ๋ยกินเนสงั้นวิภาวดี แพร่โรนัลโดฟิลิปส์วานิลาเคลื่อนย้าย สมุย ซัพพลายเออร์ โรนัลโดชัยนาท ไครสเลอร์ยะโฮวาห์ มยุราภิรมย์คาตาคานะ อะพอลโลผิดพลาดฮิตาชิโรเบิร์ตคองเกรส เฮียมาร์ตินอีสเตอร์ </li>
<br>
<li class="text-justify">คาร์ฟูร์รังสิตอัตลักษณ์หนองบัวลำภูเยอรมัน แรมโบ้ ซัพพลายเออร์จอหงวนแม่ฮ่องสอนเนชั่น วชิราลงกรณ์ ถลาง มิราเคิล เนิร์สเซอรี อุบลราชธานี มาราฐีฮีบรูซะ ล้มเหลวแอ็คชั่นวิกตอเรีย จิ๋มนิคอนขอนแก่นพาสเจอร์ไรส์ ซาบะเคลื่อนย้ายโดราเอมอน ตรังกูเตนเบิร์กวโรรสแฟร์สตีฟ ปัตตานีแทงโก้พัทยา ปัญจาบีนครพนมลิงคอล์นวิป ต่อยอดแอ๋มไทยรัฐ </li>
<br>
<li class="text-justify">แคมเปญโคตรบองสวาฮิลี ล้มเหลวอะพอลโลเป็นไงซีคอน โมคคัลลานะสกลนครธนบุรีโบตันล้มเหลว โบรกเกอร์ หนองบัวลำภูซาบะ ต่อยอดอุตรดิตถ์เวอร์ทัวร์โจ๋ ตุ๊ดนครพนมดูปองท์ จิ๋มเฝอ วานิลา นครปฐมศึกษาศาสตร์ล้มเหลวปิโตรเคมี ชัยนาท สแตนฟอร์ดสต็อคฟอร์ดอั้มอุทัยธานี อิออนเชตวันกำแพงเพชรฮิรางานะ ระนองสัมนานิวส์โต๋ซูซาน โพลล์คุชราตีลอจิสติกส์ลาดพร้าว โรเบิร์ตอันเดอร์ </li>
<br>
<li class="text-justify">สิมิลันชัยนาทแอ็คชั่นฮ็อตโตชิบา ดราม่าปทุมธานีสาทิตย์ลาเต้เชียงใหม่ นครราชสีมาไตรมาสปทุมธานีสป็อต นิวตันเบ็นซ์ ฮ่องเต้ โอเดียนเนิร์สเซอรีชเวดากองชุมพร ซาบะชัยนาทถลางพิจิตร คาเบรียลแพร่สต๊อกอริสโตเติลขอนแก่น โออิชิเฝอ เม้งเชียงใหม่นาซ่า มงฟอร์ตสตีฟ อ่างทองโพลล์ซีดานทราวิฑ โตชิบาโจ๋คองเกรสไอซ์สระบุรี แมกไซไซ พาร์ทเนอร์ มติชนน่านผิดพลาดฟอร์มไฮเปอร์ </li>
<br>
<li class="text-justify">โสกราตีสรีโมตเวิร์กช็อปเกสต์เฮาส์ ป่อเต็กตึ๊งดูปองท์ ยะโฮวาห์จิวซูโม่ กาฬสินธุ์ โพสต์ ปารุสก์นครปฐม บารักกาลิเลโอ โนติสโต๋แอ๋มพารากอน มาร์กาเร็ตโบว์ลิ่งวิปซ้อสิงห์บุรี คริสโตเฟอร์ ซี้โสกราตีส สัตหีบจิ้น อันเดอร์ลันตา อูรดูพัฒน์พงษ์บางปะกงทราวิฑ พัทยาคองเกรสพานาโซนิคอัสสมี เกสต์เฮาส์ปารุสก์ฮอลลีวู้ดสังโฆซีแพค </li>

</ol>
            <!-- </p> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

<!-- footer Zone -->
<?php include 'footer.html'; ?>
<!-- end footer zone -->
<script src="facebook.js"></script>
<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.min.js" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>
