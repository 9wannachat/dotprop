<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <script src="js/dropzone.js"></script>
    <link rel="stylesheet" href="css/dropzone.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Dotprop</title>
</head>

<body>
    <header>
        <?php include 'header.php'; ?>
    </header>
    <main class="role">
        <div class="container py-4">
            <h2 class="sarabun py-4">อัพโหลดรูปภาพ</h2>
            <hr>

            <form action="announceimage.php?id=<?php echo $_GET['id']; ?>" class="dropzone" id="dropzonewidget"></form><br>

            <div class="d-flex justify-content-end">
                <a href="profile.php" class="btn btn-info">กลับ</a>
            </div>
        </div>
    </main>

</body>

</html>

<?php
include 'condb/condb.php';
ob_start();

function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


    if(isset($_FILES["file"])){
       
        $id = $_GET['id'];

        $request = 1;
        if(isset($_POST['request'])){ 
          $request = $_POST['request'];
        }
        
        // Upload file
        if($request == 1){ 
        
            $file = $_FILES["file"];
            $fileName = $file['name'];
            $filTmpName = $file['tmp_name'];


            date_default_timezone_set("Asia/Bangkok");
            $now2 = new DateTime();
            $time2 = ($now2->format('dmY')); 
        
        
            $fileExt = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExt));
            $fileNameNew = $time2 ."".generateRandomString()."." . $fileActualExt;
            $fileDestination = 'upload/' . $fileNameNew;
        
        
          $msg = ""; 
          if ( move_uploaded_file($filTmpName, $fileDestination)) {
            $input = "INSERT INTO propimage (img_prop,img_name) VALUES ($id,'$fileNameNew')";

            if($conn->query($input)==true){
                echo '<script>alert("อัพโหลดภาพสำเร็จ")</script>';
            }
            
          }else{    
            $msg = "Error while uploading"; 
          } 
          echo $msg;
          exit;
        }
        
        // Remove file
        if($request == 2){ 
          $filename = $target_dir.$_POST['name'];  
          unlink($filename); 
          exit;
        }
    }

?>