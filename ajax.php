<?php
include 'condb/condb.php';
ob_start();

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


$target_dir = "upload/"; // Upload directory

$request = 1;
if(isset($_POST['request'])){ 
  $request = $_POST['request'];
}

// Upload file
if($request == 1){ 

    $file = $_FILES["file"];
    $fileName = $file['name'];
    $filTmpName = $file['tmp_name'];


    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $fileNameNew = $time2 ."".generateRandomString()."." . $fileActualExt;
    $fileDestination = 'upload/' . $fileNameNew;

    
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
  $msg = ""; 
  if ( move_uploaded_file($filTmpName, $fileDestination)) {
    $input = "INSERT INTO propimage (img_prop,img_name) VALUES ";
    $msg = "Successfully uploaded"; 
  }else{    
    $msg = "Error while uploading"; 
  } 
  echo $msg;
  exit;
}

// Remove file
if($request == 2){ 
  $filename = $target_dir.$_POST['name'];  
  unlink($filename); 
  exit;
}