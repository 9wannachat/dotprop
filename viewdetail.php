<?php
    session_start();
    include 'condb/condb.php';


    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $edit  = "SELECT * FROM proppost  inner join properties on properties.prop_id = proppost.post_prop 
        inner join operation on operation.op_id = properties.prop_oper
        inner join proptype on proptype.type_id = properties.prop_type
        inner join province on province.PROVINCE_ID = properties.prop_province
        inner join district on district.DISTRICT_ID = properties.prop_distric
        inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur
        inner join propimage on propimage.img_prop = proppost.post_prop where post_prop = '$id' ";
        $result = $conn->query($edit);

        while($row = $result->fetch_assoc()){
        $prop_id = $row['prop_id'];
        $prop_oper = $row['prop_oper'];
        $prop_type = $row['prop_type'];
        $prop_topic = $row['prop_topic'];
        $prop_price = $row['prop_price'];
        $prop_direct = $row['prop_direct'];
        $prop_detail = $row['prop_detail'];
        $prop_province = $row['PROVINCE_NAME'];
        $prop_amphur = $row['AMPHUR_NAME'];
        $prop_distric = $row['DISTRICT_NAME'];
        $prop_road = $row['prop_road'];
        $prop_soi = $row['prop_soi'];
        $prop_project = $row['prop_project'];
        $prop_building = $row['prop_building'];
        $prop_space = $row['prop_space'];
        $prop_bedroom = $row['prop_bedroom'];
        $prop_bathroom = $row['prop_bathroom'];
        $prop_floor= $row['prop_floor'];
        $prop_layernumber = $row['prop_layernumber'];
        $prop_highlight = $row['prop_highlight'];
        $prop_view = $row['prop_view'];
        $oper = $row['op_name'];
        $typel = $row['type_name'];

        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dotprop</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
</head>

<body onload="loadComment(<?php echo $id; ?>)">
    <header>
        <?php include 'header.php'; ?>
    </header>
    <main role="main">
        <div class="album py-5">
            <div class="container">

                <div class="card">
                    <div id="print">
                        <div class="card-header">
                            <div class="d-flex justify-content-center">
                                <h4><?php echo $prop_topic;  ?></h4>
                            </div>
                        </div><br>

                        <div class="d-flex justify-content-center">
                            <?php
                                $getImg ="SELECT * FROM propimage WHERE img_prop = '$id'";
                                $resImg = $conn->query($getImg);
                                while($res = $resImg->fetch_assoc()){
                             ?>
                            <div class="row">
                                <div class="col-4">
                                    <img class="" src="upload/<?php echo $res['img_name'];  ?> " width="400px"
                                        height="400px">
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div><br>
                        <div class="d-flex justify-content-center">
                            <div class="card">
                                <div class="card-body">
                                    <b>คำอธิบาย</b>
                                    <p><?php echo $prop_detail; ?></p>
                                    <b><?php echo $typel." ".$oper; ?></b>
                                    <p>ราคา : <?php echo $prop_price; ?> บาท</p>
                                    <p> <strong>ที่ตั้ง</strong> ตำบล : <?php echo $prop_distric;?> อำเภอ :
                                        <?php echo $prop_amphur;?> จังหวัด : <?php echo $prop_province;?> ถนน :
                                        <?php echo $prop_road;?> ซอย : <?php echo $prop_soi;?></p>

                                    <b>รายละเอียด</b><br>
                                    <p><strong>ชื่อโครงการ</strong> <?php echo $prop_project;?></p>
                                    <p><strong>พื้นที่</strong> <?php echo $prop_space;?> ตารางเมตร</p>
                                    <p><strong>ห้องนอน</strong> <?php echo $prop_bedroom;?> ห้อง
                                        <strong>ห้องน้ำ</strong> <?php echo $prop_bathroom;?> ห้อง </p>
                                    <p><strong>จุดเด่น</strong> <?php echo $prop_highlight;?></p>
                                    <p><strong>วิว</strong> <?php echo $prop_view;?></p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-md">
                            <div class="card" style="margin: 10px;">
                                <div class="card-body">
                                    <h5 class="card-title">ความคิดเห็น</h5>

                                    <div class="card">
                                        <div class="card-body">
                                            <div id="showComment"></div>
                                        </div>
                                    </div><br>

                                    <div class="input-group flex-nowrap">
                                        <input type="hidden" name="commentId" id="commentId" value="<?php echo $id; ?>">
                                        <input type="text" class="form-control" placeholder="ความคิดเห็น......"
                                            aria-label="Comment" aria-describedby="addon-wrapping" id="comment">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" onclick="comment()"
                                                id="addon-wrapping">ส่ง</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div> <br>
                <div class="row">
                    <div class="col-md-6">
                        <a href="index.php" class="btn btn-block btn-warning">ย้อนกลับ</a>
                    </div>

                    <div class="col-md-6">
                        <button onclick="onprint()" class="btn btn-block btn-info">พิมพ์ PDF</button>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <script>
        function onprint() {
            var divContents = $("#print").html()
            var printWindow = window.open('', '', 'height=800,width=1000,headers=no,footer=no')
            printWindow.document.write('<html><head><title></title>')
            printWindow.document.write('<link rel="stylesheet" href="style.css">')
            printWindow.document.write('<link rel="stylesheet" href="css/bootstrap.min.css">')
            printWindow.document.write('<style>.position (width: 50rem;   margin-top:10rem;)</style>')
            printWindow.document.write('</head><body >')
            printWindow.document.write(divContents)
            printWindow.document.write('</body></html>')
            printWindow.document.close()
            printWindow.print()
        };

        function comment() {
            var id = document.getElementById("commentId").value
            var comment = document.getElementById("comment").value

            console.log(id + " " + comment )

            $.ajax({
                url: "postComment.php",
                method: "POST",
                data: { query: id , comment:comment},
                success: function (data) {
                    var objData = JSON.parse(data)                 
                    console.log(objData)

                    if(objData.statusCode == 400){
                        alert("กรุณา Login เพื่อยืนยันตัวตนก่อน");
                    }

                    document.getElementById("comment").value=""
                    loadComment(id);
                }
            })
        }

        function loadComment(id) {
            console.log(id);
            $.ajax({
                url: "loadComment.php",
                method: "POST",
                data: { query: id },
                success: function (data) {
                    document.getElementById("showComment").innerHTML = data;
                }
            })
        }

    </script>
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</body>

</html>