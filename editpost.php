<?php
  include 'condb/condb.php';
  session_start();
  ob_start();

  if(isset($_GET['id'])){
    $id = $_GET['id'];

    $sqlGetMarker = "SELECT prop_id,prop_lat,prop_lng,prop_price FROM properties where prop_id = '$id'"; 

  $resultGetMarker = $conn->query($sqlGetMarker);

  $i=0;
  $arr= array();

  if($resultGetMarker->num_rows > 0){
    while($resultMarker = $resultGetMarker->fetch_assoc()){
      $i++;
      $latLng = array();

      $latLng['id'] = $resultMarker["prop_id"];
      $latLng['price'] = $resultMarker["prop_price"];
      $latLng['lat'] = $resultMarker["prop_lat"];
      $latLng['lng'] = $resultMarker["prop_lng"];

      $arr[$i] = $latLng;

    }
  }
   $JSONval = json_encode($arr);


    $edit  = "SELECT * FROM proppost  inner join properties on properties.prop_id = proppost.post_prop 
                                      inner join propimage on propimage.img_prop = proppost.post_prop where post_prop = '$id' ";
    $result = $conn->query($edit);
    while($row = $result->fetch_assoc()){
      $prop_oper = $row['prop_oper'];
      $prop_type = $row['prop_type'];
      $prop_topic = $row['prop_topic'];
      $prop_price = $row['prop_price'];
      $prop_direct = $row['prop_direct'];
      $prop_detail = $row['prop_detail'];
      $prop_province = $row['prop_province'];
      $prop_amphur = $row['prop_amphur'];
      $prop_distric = $row['prop_distric'];
      $prop_road = $row['prop_road'];
      $prop_soi = $row['prop_soi'];
      $prop_project = $row['prop_project'];
      $prop_building = $row['prop_building'];
      $prop_space = $row['prop_space'];
      $prop_bedroom = $row['prop_bedroom'];
      $prop_bathroom = $row['prop_bathroom'];
      $prop_floor= $row['prop_floor'];
      $prop_layernumber = $row['prop_layernumber'];
      $prop_highlight = $row['prop_highlight'];
      $prop_view = $row['prop_view'];
    }
  }
 
  $oper = "SELECT * FROM operation ";
  $type = "SELECT * FROM proptype ";
  $direct = "SELECT * FROM direction";
  $province = "SELECT * FROM province";
  $amphur = "SELECT * FROM amphur";
  $district = "SELECT * FROM district";

  $con_oper = $conn->query($oper);
  $con_type = $conn->query($type);
  $con_direct = $conn->query($direct);
  $con_pro = $conn->query($province);
  $con_amphur = $conn->query($amphur);
  $con_dist = $conn->query($district);

  $numberType = $con_type->num_rows;

  function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Dotprop</title>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="css/dotprop.css">
  <link rel="stylesheet" href="css/all.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/carousel.css">
  <link rel="stylesheet" href="css/megamenu.css">
  <link rel="stylesheet" href="css/modalsb.css">
  <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">

</head>

<body>
  <header>

  </header>

  <main role="main">
    <div class="container py-4">
      <h2 class="sarabun py-4">แบบฟอร์มกรอกรายละเอียด</h2>
      <hr>
      <form method="POST" class="sarabun" enctype="multipart/form-data">
        <div class="form-row">
          <div class="form-group col-md-3">
            <label class="my-1" for="type">ทำรายการ</label>
            <select class="custom-select my-1" name="operation" id="type">

              <?php
              while($res_oper = $con_oper->fetch_assoc()){
                ?>
              <option value="<?php echo $res_oper['op_id']; ?>"
                <?php if($res_oper['op_id'] == $prop_oper){echo "selected"; } ?>><?php echo $res_oper['op_name']; ?>
              </option>
              <?php
              }
            ?>
            </select>
          </div>
          <div class="form-group col-md-3">
            <label class="my-1" for="estate-type">ประเภททรัพย์</label>
            <select class="custom-select my-1" name="type" id="estate-type">
              <?php           
              while($res_type = $con_type->fetch_assoc()){
                ?>
              <option value="<?php echo $res_type['type_id']; ?>"
                <?php if($res_type['type_id'] == $prop_type){echo "selected"; } ?>><?php echo $res_type['type_name']; ?>
              </option>
              <?php
              }
            ?>
              <option value="<?php echo $numberType+1; ?>">อื่นๆ</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label class="my-1" for="estate-type">ประเภททรัพย์</label>
            <input type="text" id="estate-type-custom" name="other" class="form-control my-1">
          </div>
        </div>

        <div class="form-group row">
          <label class="my-1 col-md-2" for="estate-type">หัวข้อประกาศ</label>
          <div class="col-md-10">
            <input type="text" name="topic" id="estate-type-custom" class="form-control my-1"
              value="<?php echo $prop_topic; ?>">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-7">
            <label for="price" class="my-1">ราคา</label>
            <input type="text" name="price" id="price" class="form-control my-1" value="<?php echo $prop_price; ?>">
          </div>
          <div class="form-group col-md-5">
            <label for="direction" class="my-1">ทิศ</label>
            <select class="custom-select my-1 mr-sm-2" name="direct" id="direction">

              <?php
              while($res_direct = $con_direct->fetch_assoc()){
                ?>
              <option value="<?php echo $res_direct['direct_id']; ?>"
                <?php if($res_direct['direct_id'] == $prop_direct){echo "selected"; } ?>>
                <?php echo $res_direct['direct_name']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
        </div>

        <label for="estate-detail" class="my-1">รายละเอียดทรัพย์</label>
        <textarea name="estate-detail" id="estate-detail" cols="30" rows="10"
          class="form-control my-1"><?php echo $prop_detail; ?></textarea>

        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="province" class="my-1">จังหวัด</label>
            <select class="custom-select my-1 mr-sm-2" name="province" id="province">

              <?php
              while($res_prov = $con_pro->fetch_assoc()){
                ?>
              <option value="<?php echo $res_prov['PROVINCE_ID']; ?>"
                <?php if($res_prov['PROVINCE_ID'] == $prop_province){echo "selected"; } ?>>
                <?php echo $res_prov['PROVINCE_NAME']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="district" class="my-1">อำเภอ</label>
            <select class="custom-select my-1 mr-sm-2" name="amphur" id="district">

              <?php
              while($res_amp = $con_amphur->fetch_assoc()){
                ?>
              <option value="<?php echo $res_amp['AMPHUR_ID']; ?>"
                <?php if($res_amp['AMPHUR_ID'] == $prop_amphur){echo "selected"; } ?>>
                <?php echo $res_amp['AMPHUR_NAME']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="sub-district" class="my-1">ตำบล</label>
            <select class="custom-select my-1 mr-sm-2" name="district" id="sub-district">

              <?php
              while($res_dist = $con_dist->fetch_assoc()){
                ?>
              <option value="<?php echo $res_dist['DISTRICT_ID']; ?>"
                <?php if($res_dist['DISTRICT_ID'] == $prop_distric){echo "selected"; } ?>>
                <?php echo $res_dist['DISTRICT_NAME']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="road" class="my-1">ถนน</label>
            <input type="text" name="road" id="road" class="form-control my-1" value="<?php echo $prop_road; ?>">
          </div>
          <div class="form-group col-md-2">
            <label for="soi" class="my-1">ซอย</label>
            <input type="text" name="soi" id="soi" class="form-control my-1" value="<?php echo $prop_soi; ?>">
          </div>
          <div class="form-group col-md-4">
            <label for="project-name" class="my-1">ชื่อโครงการ</label>
            <select class="custom-select my-1 mr-sm-2" name="project" id="project-name">
              <option value="<?php echo $prop_project ?>" selected><?php echo $prop_project ?></option>
              <option value="ไม่มี">ไม่มี</option>
              <option value="พลัสคอนโด (Plus Condo)">พลัสคอนโด (Plus Condo)</option>
              <option value="เดอะไรส์ เรสซิเดนท์ (The Rise Residence)">เดอะไรส์ เรสซิเดนท์ (The Rise Residence)</option>
            </select>
          </div>
          <div class="form-group col-md-2">
            <label for="building" class="my-1">อาคาร</label>
            <input type="text" name="building" id="building" class="form-control my-1"
              value="<?php echo $prop_building; ?>">
          </div>
          <div class="form-group col-md-2">
            <label for="area" class="my-1">พื้นที่ตารางเมตร</label>
            <input type="text" name="area" id="area" class="form-control my-1" value="<?php echo $prop_space; ?>">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="bedroom" class="my-1">จำนวนห้องนอน</label>
            <input type="text" name="bedroom" id="bedroom" class="form-control my-1"
              value="<?php echo $prop_bedroom; ?>">
          </div>
          <div class="form-group col-md-3">
            <label for="bathroom" class="my-1">จำนวนห้องน้ำ</label>
            <input type="text" name="bathroom" id="bathroom" class="form-control my-1"
              value="<?php echo $prop_bathroom; ?>">
          </div>
          <div class="form-group col-md-3">
            <label for="floor" class="my-1">อยู่ชั้นที่</label>
            <input type="text" name="floor" id="floor" class="form-control my-1" value="<?php echo $prop_floor; ?>">
          </div>
          <div class="form-group col-md-3">
            <label for="allfloor" class="my-1">จำนวนชั้นทั้งหมด</label>
            <input type="text" name="allfloor" id="allfloor" class="form-control my-1"
              value="<?php echo $prop_layernumber; ?>">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-8">
            <label for="highlight">จุดเด่นของอสังหาฯ</label>
            <input type="text" name="highlight" id="highlight" class="form-control my-1"
              value="<?php echo $prop_highlight; ?>">
          </div>
          <div class="form-group col-md-4">
            <label for="view">วิว</label>
            <select class="custom-select my-1 mr-sm-2" name="view" id="view">
              <option value="<?php echo $prop_view ?>" selected><?php echo $prop_view ?></option>
              <option value="ไม่มี">ไม่มี</option>
              <option value="วิวเมือง">วิวเมือง</option>
              <option value="วิวธรรมชาติ">วิวธรรมชาติ</option>
            </select>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <div class="card">
              <div class="card-header">

                <div class="row">
                  <div class="col-md-6">
                    <p><strong>ระบุตำแหน่งบน Google Map</strong></p>
                  </div>
                  <div class="col-md-6">
                    <div class="d-flex justify-content-end">
                      <a class="btn btn-outline-danger" onclick="unMarker()">ลบหมุด</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <!-- Key GoogleMaps ==> AIzaSyB2pDj3lbScNYKRBsO6K1QcTc24OASU5J4 -->

                <div id="googleMap" style="width:100%;height:400px;"></div>
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">

              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <button type="submit" name="submit" class="btn btn-primary sarabun btn-block my-1">บันทึก</button>
          </div>
          <div class="col-md-4">
            <a href="#" class="btn btn-success btn-block sarabun my-1">แก้ไขภาพ</a>
          </div>
          <div class="col-md-4">
            <a href="profile.php" class="btn btn-warning btn-block sarabun my-1">ย้อนกลับ</a>
          </div>
        </div>

      </form>
    </div>
  </main>

  <!-- footer Zone -->

  <!-- end footer zone -->

  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  <script>
    var dataJson = <?php echo $JSONval; ?>

      function myMap() {
        var count = Object.keys(dataJson).length

        var mapProp = {
          center: new google.maps.LatLng(13.7649, 100.5383),
          zoom: 6,
        }

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp)

        for (let i = 1; i <= count; i++) {
          var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: { lat: parseFloat(dataJson[i].lat), lng: parseFloat(dataJson[i].lng) }
          })
          marker.setMap(map)
        }
      }

    function unMarker() {

      i = 0

      var mapProp = {
        center: new google.maps.LatLng(13.7649, 100.5383),
        zoom: 6,
      };
      let myLatlng = { lat: sessionStorage.getItem("lat"), lng: sessionStorage.getItem("lng") }
      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp)
      var marker = new google.maps.Marker({
        position: myLatlng
      });



      marker.setMap(null)
      // myMap()

      google.maps.event.addListener(map, 'click', function (event) {


        let jsonLatLng = event.latLng.toJSON()
        console.log("lat : " + jsonLatLng.lat)
        console.log("lng : " + jsonLatLng.lng)

        document.getElementById("lat").value = jsonLatLng.lat
        document.getElementById("lng").value = jsonLatLng.lng

        var marker = new google.maps.Marker({
          position: event.latLng,
          map: map
        });
      })


    }


  </script>
  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2pDj3lbScNYKRBsO6K1QcTc24OASU5J4&callback=myMap"></script>
</body>

</html>
<?php        

        if(isset($_POST['submit'])){     
      
          $lat = $_POST['lat'];
          $lng = $_POST['lng'];
          $operation = $_POST['operation'];
          $es_type = $_POST['type'];
          // $other = $_POST['other']; //----- add to new  type
          $topic = $_POST['topic'];
          $price = $_POST['price'];
          $direction = $_POST['direct'];
          $detail = $_POST['estate-detail'];
          $prov = $_POST['province'];
          $dist = $_POST['district'];
          $amp = $_POST['amphur'];
          $road = $_POST['road'];
          $soi = $_POST['soi'];
          $project = $_POST['project'];
          $build = $_POST['building'];
          $space = $_POST['area'];
          $bedroom = $_POST['bedroom'];
          $bathroom = $_POST['bathroom'];
          $floor = $_POST['floor'];
          $allfloor = $_POST['allfloor'];
          $highlight = $_POST['highlight'];
          $view = $_POST['view'];

          $update = "UPDATE properties SET prop_oper ='$operation',prop_type ='$es_type',prop_topic='$topic',prop_price='$price',prop_direct='$direction',prop_detail='$detail',
                                          prop_province='$prov',prop_amphur='$amp',prop_distric='$dist',prop_road='$road',prop_soi='$soi',prop_project='$project',prop_building='$build',
                                          prop_space='$space',prop_bedroom='$bedroom',prop_bathroom='$bathroom',prop_floor='$floor',prop_layernumber='$allfloor',prop_highlight='$highlight',prop_view='$view',prop_lat = '$lat',prop_lng='$lng' where prop_id = '$id' ";
          if($conn->query($update)==true){
            echo '<script>alert("อัพเดทเรียบร้อย");</script>';  
            header("Refresh:0,url=profile.php");
          }        
          
        }

?>