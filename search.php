<?php
    include 'condb/condb.php';
    $count_num = 0;
    $a=0;
    if(isset($_POST['submit'])){
        $keyword = $_POST['keyword'];
        $typeG = $_POST['type'];
        $provinceG = $_POST['province'];
        $room = $_POST['room'];
        $area = $_POST['area'];
        $room2 = $_POST['room2'];
        $price = $_POST['price'];

        //echo $keyword.'<br>';
        // echo $typeG.'<br>';
        // echo $provinceG.'<br>';
        // echo $room.'<br>';
        // echo $room2.'<br>';
        // echo $area.'<br>';
        //echo $price.'<br>';
        
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dotprop</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <style>
      .fix_line{
      
        line-height:1.2em;
        height:3.6em;
        overflow:hidden;
      
      }
    </style>
</head>
<body>
<header>
    <?php           
        include 'header.php';            
    ?>
</header>
    <main role="main">
        <div class="album py-5">
            <div class="container">
                <?php
                    if($keyword==null){ 
                        if($typeG != '0' AND $provinceG != '0' AND $room != '0'  AND $room2 != '0'){                            

                            $getsel = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                                                inner join proppost on proppost.post_prop = properties.prop_id
                                                                inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                                                inner join operation on operation.op_id = properties.prop_oper
                                                                inner join proptype on proptype.type_id = properties.prop_type 
                                                               ";
                            $resPost = $conn->query($getsel);                           
                            ?>
                            <div class="row">
                                <?php
                                        while($res = $resPost->fetch_assoc()){
                                        // print_r($res);                                         
                                        if($res['post_status'] != 002 AND $res['post_verify'] != 003){ 
                                            $a++;                                      
                                            if($res['PROVINCE_ID'] == $provinceG && $res['prop_type']==$typeG && $res['prop_bedroom']==$room && $res['prop_bathroom']==$room2 ){ 
                                                $exArea = explode("-",$area);                                               
                                                $Gspace = $res['prop_space'];                                           
                                                if( $exArea[0] >= $Gspace || $Gspace <= $exArea[1] ){ 
                                                    $exPrice = explode("-",$price);                                                  
                                                    if($exPrice[0] >= $res['prop_price'] || $res['prop_price'] <= $exPrice[1] ){                                           
                                                        $propId =  $res['prop_id'];
                                                        $count_num++;
                                                        $getImg = "SELECT * FROM propimage WHERE img_prop = $propId ORDER BY img_id DESC LIMIT 1";
                                                        $resImg = $conn->query($getImg);
                                                        $Img = $resImg->fetch_assoc();
                                    ?>
                                        <div class="col-md-4">
                                        <div class="card mb-4 shadow-sm">
                                            <img class="card-img-top" src="upload/<?php echo $Img['img_name']; ?>" alt="Card image cap">
                                            <div class="card-body">
                                            <h5 class="card-title"><?php echo $res['prop_topic']; ?></h5>
                                            <p class="card-text fix_line "><?php echo $res['prop_detail']; ?></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                <div class="text-center">
                                                    <a href="viewdetail.php?id=<?php echo $res['prop_id'];  ?>" class="th-head btn btn-light btn-block"><?php echo $res['op_name'];?></a>
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="text-center">
                                                    <a href="#" class="th-head btn btn-light btn-block"><?php echo $res['prop_space'];?> ตร.ม.</a>
                                                </div> 
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                </div>
                                    <?php
                                                    }else{

                                                    }
                                                }else{

                                                }
                                            }else{        
                                                                                     
                                                if($a == 1 && $count_num == 0){
                                                //    No Echo 
                                                }else if($count_num == 0){
                                                    echo "ไม่มีข้อมูล";
                                                }
                                            }
                                        }
                                        }
                                    ?>    
                            </div>
                        <?php
                        }else if($typeG = '3' AND $provinceG != '0'){
                           
                            $getsel = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                                                inner join proppost on proppost.post_prop = properties.prop_id
                                                                inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                                                inner join operation on operation.op_id = properties.prop_oper
                                                                inner join proptype on proptype.type_id = properties.prop_type 
                                                               ";
                            $resPost = $conn->query($getsel);                           
                            ?>
                            <div class="row">
                                <?php
                                        while($res = $resPost->fetch_assoc()){                                     
                                        if($res['post_status'] != 002 AND $res['post_verify'] != 003){ 
                                            $a++;                                      
                                            if($res['PROVINCE_ID'] == $provinceG && $res['prop_type']==$typeG ){                                             
                                                    $exPrice = explode("-",$price);                                                  
                                                    if($exPrice[0] >= $res['prop_price'] || $res['prop_price'] <= $exPrice[1] ){                                           
                                                        $propId =  $res['prop_id'];
                                                        $count_num++;
                                                        $getImg = "SELECT * FROM propimage WHERE img_prop = $propId ORDER BY img_id DESC LIMIT 1";
                                                        $resImg = $conn->query($getImg);
                                                        $Img = $resImg->fetch_assoc();
                                    ?>
                                        <div class="col-md-4">
                                        <div class="card mb-4 shadow-sm">
                                            <img class="card-img-top" src="upload/<?php echo $Img['img_name']; ?>" alt="Card image cap">
                                            <div class="card-body">
                                            <h5 class="card-title"><?php echo $res['prop_topic']; ?></h5>
                                            <p class="card-text fix_line "><?php echo $res['prop_detail']; ?></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                <div class="text-center">
                                                    <a href="viewdetail.php?id=<?php echo $res['prop_id'];  ?>" class="th-head btn btn-light btn-block"><?php echo $res['op_name'];?></a>
                                                </div>
                                                </div>
                                                <div class="col-md-6">
                                                <div class="text-center">
                                                    <a href="#" class="th-head btn btn-light btn-block"><?php echo $res['prop_space'];?> ตร.ม.</a>
                                                </div> 
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                </div>
                                    <?php
                                                    }else{

                                                    }
                                           
                                            }else{        
                                                                                     
                                                if($a == 1 && $count_num == 0){
                                                //    No Echo 
                                                }else if($count_num == 0){
                                              
                                                   
                                                }
                                            }
                                        }
                                        }
                                    ?>    
                            </div>
                            <?php
                        }else{
                            $select = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                                                inner join proppost on proppost.post_prop = properties.prop_id
                                                                inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                                                inner join operation on operation.op_id = properties.prop_oper
                                                                inner join proptype on proptype.type_id = properties.prop_type ";
                            $resPost = $conn->query($select);
                            ?>
                                <div class="row">
                                    <?php
                                            while($res = $resPost->fetch_assoc()){
                                            if($res['post_status'] != 002 AND $res['post_verify'] != 003){
                                                $propId =  $res['prop_id'];
                                            
                                                $getImg = "SELECT * FROM propimage WHERE img_prop = $propId ORDER BY img_id DESC LIMIT 1";
                                                $resImg = $conn->query($getImg);
                                                $Img = $resImg->fetch_assoc();
                                        ?>
                                            <div class="col-md-4">
                                            <div class="card mb-4 shadow-sm">
                                                <img class="card-img-top" src="upload/<?php echo $Img['img_name']; ?>" alt="Card image cap">
                                                <div class="card-body">
                                                <h5 class="card-title"><?php echo $res['prop_topic']; ?></h5>
                                                <p class="card-text fix_line "><?php echo $res['prop_detail']; ?></p>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="text-center">
                                                        <a href="viewdetail.php?id=<?php echo $res['prop_id'];  ?>" class="th-head btn btn-light btn-block"><?php echo $res['op_name'];?></a>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="text-center">
                                                        <a href="#" class="th-head btn btn-light btn-block"><?php echo $res['prop_space'];?> ตร.ม.</a>
                                                    </div> 
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                    </div>
                                        <?php
                                            }
                                            }
                                        ?>    
                                </div>
                            <?php
                        }          
                    
                    }else{
                        $select = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                                                inner join proppost on proppost.post_prop = properties.prop_id
                                                                inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                                                inner join operation on operation.op_id = properties.prop_oper
                                                                inner join proptype on proptype.type_id = properties.prop_type where prop_topic like '%".$keyword."%' ";
                            $resPost = $conn->query($select);
                            ?>
                                <div class="row">
                                    <?php
                                            while($res = $resPost->fetch_assoc()){
                                            if($res['post_status'] != 002 AND $res['post_verify'] != 003){
                                                $propId =  $res['prop_id'];
                                            
                                                $getImg = "SELECT * FROM propimage WHERE img_prop = $propId ORDER BY img_id DESC LIMIT 1";
                                                $resImg = $conn->query($getImg);
                                                $Img = $resImg->fetch_assoc();
                                        ?>
                                            <div class="col-md-4">
                                            <div class="card mb-4 shadow-sm">
                                                <img class="card-img-top" src="upload/<?php echo $Img['img_name']; ?>" alt="Card image cap">
                                                <div class="card-body">
                                                <h5 class="card-title"><?php echo $res['prop_topic']; ?></h5>
                                                <p class="card-text fix_line "><?php echo $res['prop_detail']; ?></p>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <div class="text-center">
                                                        <a href="viewdetail.php?id=<?php echo $res['prop_id'];  ?>" class="th-head btn btn-light btn-block"><?php echo $res['op_name'];?></a>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <div class="text-center">
                                                        <a href="#" class="th-head btn btn-light btn-block"><?php echo $res['prop_space'];?> ตร.ม.</a>
                                                    </div> 
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                    </div>
                                        <?php
                                            }
                                            }
                                        ?>    
                                </div>
                            <?php
                    }
                ?>
            </div>
        </div>
    </main>
<script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>