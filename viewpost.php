<?php
    include 'condb/condb.php';
    if(isset($_GET['id'])){
        $id = $_GET['id'];
  
        $getPost = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                            inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                            inner join proppost on proppost.post_prop = properties.prop_id
                                            inner join operation on operation.op_id = properties.prop_oper
                                            inner join proptype on proptype.type_id = properties.prop_type where prop_province = '$id' ";
        $resPost = $conn->query($getPost);
    }else if(isset($_GET['op']) AND isset($_GET['tp'])){
        $op = $_GET['op'];
        $tp = $_GET['tp'];

        $getPost = "SELECT * FROM properties inner join province on province.PROVINCE_ID = properties.prop_province
                                            inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                            inner join proppost on proppost.post_prop = properties.prop_id
                                            inner join operation on operation.op_id = properties.prop_oper
                                            inner join proptype on proptype.type_id = properties.prop_type where prop_oper = '$op' AND prop_type = '$tp' ";
        $resPost = $conn->query($getPost);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dotprop</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <style>
    p{
        line-height:1.2em;
        height:3.6em;
        overflow:hidden;
        }
    </style>
</head>
<body>
<header>
    <?php
            
            include 'header.php'; 
    ?>
</header>
<main role="main">
    <div class="album py-5">
        <div class="container">
            <div class="row">
                <?php
                    while($res = $resPost->fetch_assoc()){
                    if($res['post_status'] != 002 AND $res['post_verify'] != 003){
                        $propId =  $res['prop_id'];
                        $getImg = "SELECT * FROM propimage WHERE img_prop = '$propId' ORDER BY img_id DESC LIMIT 1";
                        $resImg = $conn->query($getImg);
                        $Img = $resImg->fetch_assoc();
                ?>
                    <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img class="card-img-top" src="upload/<?php echo $Img['img_name']; ?>" alt="Card image cap" width="200px" height="200px">
                        <div class="card-body">
                        <h5 class="card-title"><?php echo $res['prop_topic']; ?></h5>
                        <p class="card-text"><?php echo $res['prop_detail']; ?></p>
                        <div class="row">
                            <div class="col-md-6">
                            <div class="text-center">
                            <a href="viewdetail.php?id=<?php echo $res['prop_id'];  ?>" class="th-head btn btn-light btn-block"><?php echo $res['op_name'];?></a>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="text-center">
                                <a href="#" class="th-head btn btn-light btn-block"><?php echo $res['prop_space'];?> ตร.ม.</a>
                            </div> 
                            </div>
                        </div>
                        </div>
                    </div>
            </div>
                <?php
                        }
                    }
                ?>
                    
            </div>
    </div>
</main>
<script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>