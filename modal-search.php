<?php
  $province = "SELECT * FROM province";
  $type = "SELECT * FROM proptype ";

  $con_pro = $conn->query($province);
  $con_type = $conn->query($type);
?>
<div class="modal right fade" id="searchbar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">

  <div class="modal-header">
    <h2 class="modal-title th-head" id="myModalLabel2">ค้นหา ที่พักโดนใจ ที่นี่จ้า!!</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>

  <div class="modal-body">
    <form action="search.php" method="POST">
      <div class="form-group">
        <label for="keyword">ค้นหา</label>
        <input type="text" name="keyword" id="keyword" class="form-control" placeholder="ค้นหา">
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="type">ประเภท</label>
          <select name="type" id="type" class="form-control">
            <option value="0" selected>ทุกประเภท</option>
            <?php           
              while($res_type = $con_type->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_type['type_id']; ?>"><?php echo $res_type['type_name']; ?></option>               
                <?php
              }
            ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="province">จังหวัด</label>
          <select name="province" id="province" class="form-control">
            <option value="0" selected>ทุกจังหวัด</option>
            <?php
              while($res_prov = $con_pro->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_prov['PROVINCE_ID']; ?>"><?php echo $res_prov['PROVINCE_NAME']; ?></option>               
                <?php
              }
            ?>
          </select>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="room">จำนวนห้องนอน</label>
          <select name="room" id="room" class="form-control">
            <option value="0" selected>Any</option>
            <option value="1">1</option>                     
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="area">ตร.ม.</label>
          <select name="area" id="area" class="form-control">
            <option value="0" selected>Any</option>           
            <option value="25-30">25-30</option>            
          </select>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-6">
          <label for="room2">จำนวนห้องน้ำ</label>
          <select name="room2" id="room2" class="form-control">
            <option value="0" selected>Any</option>
            <option value="1">1</option>         
            
          </select>
        </div>
        <div class="form-group col-md-6">
          <label for="price">ราคา</label>
          <select name="price" id="price" class="form-control">
            <option value="0" selected>Unlimited</option>
            <option value="50000-100000">50,000-100,000</option>          
          </select>
        </div>
      </div>

      <button class="btn btn-success btn-block" name="submit" type="submit">ค้นหา</button>

    </form>
  </div>
</div><!-- modal-content -->
</div><!-- modal-dialog -->
</div><!-- modal -->
