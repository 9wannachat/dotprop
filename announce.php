<?php
  include 'condb/condb.php';
  session_start();
  ob_start();
  if($_SESSION['cus_id']==null){
    header("Location:login.php");
  }

  $cus_id = $_SESSION['cus_id'];

  $oper = "SELECT * FROM operation ";
  $ptype = "SELECT * FROM proptype ";
  $direct = "SELECT * FROM direction";
  $province = "SELECT * FROM province";
  $amphur = "SELECT * FROM amphur";
  $district = "SELECT * FROM district";

  $con_oper = $conn->query($oper);
  $con_ptype = $conn->query($ptype);



  $con_direct = $conn->query($direct);
  $conPro = $conn->query($province);
  $con_amphur = $conn->query($amphur);
  $con_dist = $conn->query($district);

  $numberType = $con_ptype->num_rows;

  function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Dotprop</title>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="css/dotprop.css">
  <link rel="stylesheet" href="css/all.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/carousel.css">
  <link rel="stylesheet" href="css/megamenu.css">
  <link rel="stylesheet" href="css/modalsb.css">

  <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <script>
    function load_data2(query) {
      $.ajax({
        url: "localtion.php",
        method: "POST",
        data: { query2: query },
        success: function (data) {
          $('#result2').html(data);
          console.log(data);
        }
      });
    }

    function myFunction2() {
      var amphur = document.getElementById('amphur').value;
      load_data2(amphur)
    }



    function load_data(query) {
      $.ajax({
        url: "localtion.php",
        method: "POST",
        data: { query: query },
        success: function (data) {
          $('#result').html(data);
          console.log(data);
        }
      });
    }

    function myFunction() {
      var province = document.getElementById('provinceS').value;
      load_data(province);

    }

  </script>

</head>

<body>
  <header>
    <?php include 'header.php'; ?>
  </header>

  <main role="main">
    <div class="container py-4">
      <h2 class="sarabun py-4">แบบฟอร์มกรอกรายละเอียด</h2>
      <hr>
      <form method="POST" class="sarabun" enctype="multipart/form-data">
        <div class="form-row">
          <div class="form-group col-md-3">
            <label class="my-1" for="type">ทำรายการ</label>
            <select class="custom-select my-1" name="operation" id="type">
              <option selected>-- เลือกรายการ --</option>
              <?php
              while($res_oper = $con_oper->fetch_assoc()){
                ?>
              <option value="<?php echo $res_oper['op_id']; ?>"><?php echo $res_oper['op_name']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
          <div class="form-group col-md-3">
            <label class="my-1" for="estate-type">ประเภททรัพย์</label>
            <select class="custom-select my-1" name="type" id="estate-type">
              <option selected>-- เลือกประเภททรัพย์ --</option>
              <?php           
              while($res_type = $con_ptype->fetch_assoc()){
                ?>
              <option value="<?php echo $res_type['type_id']; ?>"><?php echo $res_type['type_name']; ?></option>
              <?php
              }
            ?>
              <option value="<?php echo $numberType+1; ?>">อื่นๆ</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label class="my-1" for="estate-type">ประเภททรัพย์</label>
            <input type="text" id="estate-type-custom" name="other" class="form-control my-1">
          </div>
        </div>

        <div class="form-group row">
          <label class="my-1 col-md-2" for="estate-type">หัวข้อประกาศ</label>
          <div class="col-md-10">
            <input type="text" name="topic" id="estate-type-custom" class="form-control my-1">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-7">
            <label for="price" class="my-1">ราคา</label>
            <input type="text" name="price" id="price" class="form-control my-1">
          </div>
          <div class="form-group col-md-5">
            <label for="direction" class="my-1">ทิศ</label>
            <select class="custom-select my-1 mr-sm-2" name="direct" id="direction">
              <option selected>-- เลือกทิศ --</option>
              <?php
              while($res_direct = $con_direct->fetch_assoc()){
                ?>
              <option value="<?php echo $res_direct['direct_id']; ?>"><?php echo $res_direct['direct_name']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
        </div>

        <label for="estate-detail" class="my-1">รายละเอียดทรัพย์</label>
        <textarea name="estate-detail" id="estate-detail" cols="30" rows="10" class="form-control my-1"></textarea>

        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="province" class="my-1">จังหวัด</label>
            <select class="custom-select my-1 mr-sm-2" name="province" id="provinceS" onchange="myFunction()">
              <option selected>-- เลือกจังหวัด --</option>
              <?php
              while($resProv = $conPro->fetch_assoc()){
                ?>
              <option value="<?php echo $resProv['PROVINCE_ID']; ?>"><?php echo $resProv['PROVINCE_NAME']; ?></option>
              <?php
              }
            ?>
            </select>
          </div>
          <div class="form-group col-md-4">
            <label for="amphur" class="my-1">อำเภอ</label>
            <span id="result">
              <select class="custom-select my-1 mr-sm-2" name="amphur">
                <option selected>-- เลือกอำเภอ --</option>
              </select>
            </span>
          </div>
          <div class="form-group col-md-4">
            <label for="sdistrict" class="my-1">ตำบล</label>
            <span id="result2">
              <select class="custom-select my-1 mr-sm-2" name="district" id="district">
                <option selected>-- เลือกตำบล --</option>
              </select>
            </span>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="road" class="my-1">ถนน</label>
            <input type="text" name="road" id="road" class="form-control my-1">
          </div>
          <div class="form-group col-md-2">
            <label for="soi" class="my-1">ซอย</label>
            <input type="text" name="soi" id="soi" class="form-control my-1">
          </div>
          <div class="form-group col-md-4">
            <label for="project-name" class="my-1">ชื่อโครงการ</label>
            <select class="custom-select my-1 mr-sm-2" name="project" id="project-name">
              <option selected>-- เลือกโครงการ --</option>
              <option value="ไม่มี">ไม่มี</option>
              <option value="พลัสคอนโด (Plus Condo)">พลัสคอนโด (Plus Condo)</option>
              <option value="เดอะไรส์ เรสซิเดนท์ (The Rise Residence)">เดอะไรส์ เรสซิเดนท์ (The Rise Residence)</option>
            </select>
          </div>
          <div class="form-group col-md-2">
            <label for="building" class="my-1">อาคาร</label>
            <input type="text" name="building" id="building" class="form-control my-1">
          </div>
          <div class="form-group col-md-2">
            <label for="area" class="my-1">พื้นที่ตารางเมตร</label>
            <input type="text" name="area" id="area" class="form-control my-1">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="bedroom" class="my-1">จำนวนห้องนอน</label>
            <input type="text" name="bedroom" id="bedroom" class="form-control my-1">
          </div>
          <div class="form-group col-md-3">
            <label for="bathroom" class="my-1">จำนวนห้องน้ำ</label>
            <input type="text" name="bathroom" id="bathroom" class="form-control my-1">
          </div>
          <div class="form-group col-md-3">
            <label for="floor" class="my-1">อยู่ชั้นที่</label>
            <input type="text" name="floor" id="floor" class="form-control my-1">
          </div>
          <div class="form-group col-md-3">
            <label for="allfloor" class="my-1">จำนวนชั้นทั้งหมด</label>
            <input type="text" name="allfloor" id="allfloor" class="form-control my-1">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-8">
            <label for="highlight">จุดเด่นของอสังหาฯ</label>
            <input type="text" name="highlight" id="highlight" class="form-control my-1">
          </div>
          <div class="form-group col-md-4">
            <label for="view">วิว</label>
            <select class="custom-select my-1 mr-sm-2" name="view" id="view">
              <option selected>-- เลือกวิว --</option>
              <option value="ไม่มี">ไม่มี</option>
              <option value="วิวเมือง">วิวเมือง</option>
              <option value="วิวธรรมชาติ">วิวธรรมชาติ</option>
            </select>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
            <div class="card">
              <div class="card-header">

                <div class="row">
                  <div class="col-md-6">
                    <p><strong>ระบุตำแหน่งบน Google Map</strong></p>
                  </div>
                  <div class="col-md-6">
                    <div class="d-flex justify-content-end">
                      <a class="btn btn-outline-danger" onclick="unMarker()">ลบหมุด</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <!-- Key GoogleMaps ==> AIzaSyB2pDj3lbScNYKRBsO6K1QcTc24OASU5J4 -->

                <div id="googleMap" style="width:100%;height:400px;"></div>
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">

              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <button type="submit" name="submit" class="btn btn-primary sarabun btn-block my-1">บันทึก</button>
          </div>
          <div class="col-md-4">
            <button type="reset" name="button" class="btn btn-danger sarabun btn-block my-1">ล้างข้อมูล</button>
          </div>
          <div class="col-md-4">
            <a href="profile.php" class="btn btn-warning btn-block sarabun my-1">ย้อนกลับ</a>
          </div>
        </div>

      </form>
    </div>
  </main>

  <!-- footer Zone -->

  <!-- end footer zone -->

  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  <script>
    var i = 0;

    function myMap() {

      var mapProp = {
        center: new google.maps.LatLng(13.7649, 100.5383),
        zoom: 5,
      };
      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

      google.maps.event.addListener(map, 'click', function (event) {

        i++
        if (i > 1) {
          alert("ท่านสามารถปักหมุดได้ 1 ประกาศต่อ 1 หมุดเท่านั้น")
          return
        }

        let jsonLatLng = event.latLng.toJSON()
        console.log("lat : " + jsonLatLng.lat)
        console.log("lng : " + jsonLatLng.lng)

        sessionStorage.setItem("lat", jsonLatLng.lat)
        sessionStorage.setItem("lng", jsonLatLng.lng)

        document.getElementById("lat").value = jsonLatLng.lat
        document.getElementById("lng").value = jsonLatLng.lng

        console.log(sessionStorage.getItem("lat"))
        console.log(sessionStorage.getItem("lng"))

        var marker = new google.maps.Marker({
          position: event.latLng,
          map: map
        });
      })

    }

    function unMarker() {

      i = 0

      var mapProp = {
        center: new google.maps.LatLng(13.7649, 100.5383),
        zoom: 5,
      };
      let myLatlng = { lat: sessionStorage.getItem("lat"), lng: sessionStorage.getItem("lng") }
      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp)
      var marker = new google.maps.Marker({
        position: myLatlng
      });

      sessionStorage.setItem("lat", null)
      sessionStorage.setItem("lng", null)

      marker.setMap(null)
      myMap()
    }

  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2pDj3lbScNYKRBsO6K1QcTc24OASU5J4&callback=myMap"></script>
</body>

</html>
<?php        

        if(isset($_POST['submit'])){     

          $let = $_POST['lat'];
          $lng = $_POST['lng'];
          $operation = $_POST['operation'];
          $es_type = $_POST['type'];
          // $other = $_POST['other']; //----- add to new  type
          $topic = $_POST['topic'];
          $price = $_POST['price'];
          $direction = $_POST['direct'];
          $detail = $_POST['estate-detail'];
          $prov = $_POST['province'];
          $dist = $_POST['district'];
          $amp = $_POST['amphur'];
          $road = $_POST['road'];
          $soi = $_POST['soi'];
          $project = $_POST['project'];
          $build = $_POST['building'];
          $space = $_POST['area'];
          $bedroom = $_POST['bedroom'];
          $bathroom = $_POST['bathroom'];
          $floor = $_POST['floor'];
          $allfloor = $_POST['allfloor'];
          $highlight = $_POST['highlight'];
          $view = $_POST['view'];

          date_default_timezone_set("Asia/Bangkok");
          $now = new DateTime();
          $time = ($now->format('dmY'));

          $checkNumber = "SELECT * FROM proppost where post_date ='$time' AND post_member='$cus_id' ";
          $resChkNum = $conn->query($checkNumber);
          if($resChkNum->num_rows<=5){
            $checkRepeat = "SELECT * FROM properties inner join proppost on proppost.post_prop = properties.prop_id where prop_space = '$space' AND prop_price = '$price' AND post_member='$cus_id'  ";
            $resRepeat = $conn->query($checkRepeat);
            if($resRepeat->num_rows > 0){
              echo "<script>alert('ท่านสร้างโพสต์ไปเเล้วกรุณาตรวจสอบข้อมูล');</script>";
              header("Refresh:0,url=profile.php");
            }else{
              $numberPost = $resChkNum->num_rows;
              if($_POST['other'] != null ){
                $other = $_POST['other'];
                $insOth = "INSERT INTO proptype(type_name)VALUES('$other')";
                $conn->query($insOth);
              }

              $insProp = "INSERT INTO properties (prop_oper,prop_type,prop_topic,prop_price,prop_direct,prop_detail,prop_province,prop_amphur,prop_distric,prop_road,prop_soi,
                                                  prop_project,prop_building,prop_space,prop_bedroom,prop_bathroom,prop_floor,prop_layernumber,prop_highlight,prop_view,prop_lat,prop_lng) 
                                                  VALUES ('$operation','$es_type','$topic','$price','$direction','$detail','$prov','$amp','$dist','$road','$soi'
                                                  ,'$project','$build','$space','$bedroom','$bathroom','$floor','$allfloor','$highlight','$view','$let','$lng')";
              if($conn->query($insProp)==TRUE){
                $getLast = "SELECT * FROM properties ORDER BY prop_id DESC LIMIT 1";
                $resGet = $conn->query($getLast);
                $res = $resGet->fetch_assoc();
                $last = $res['prop_id'];
                $newPost = $numberPost+1;
                $insToPost = "INSERT INTO proppost (post_prop,post_member,post_number,post_date)VALUES('$last','$cus_id','$newPost','$time')";
                if($conn->query($insToPost) == TRUE){               
                  $conn->close();
                  // echo '<script>alert("ลงประกาศขายสำเร็จ");</script>';  
                  header("Refresh:0,url=announceimage.php?id=".$last);
                }else{
                  echo $conn->error;
                }
              }else{
                echo $conn->error;
              }
            }
          }else{
            echo "<script>alert('ท่านสร้างโพสต์ครบตามจำนวนเงื่อนไขของวันนี้เเล้ว');</script>";
            // header("Refresh:0,url=profile.php");
          }        

          
        }

?>