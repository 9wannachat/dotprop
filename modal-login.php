
<!-- login & Register modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="modal-header">
  <h4 class="modal-title th-head" id="title">ยินดีต้อนรับสู่ DotpropHatyai</h4>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div class="modal-body">

    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active th-head" id="login-tab" data-toggle="tab" href="#logintab" role="tab" aria-controls="login" aria-selected="true">ลงชื่อเข้าใช้</a>
      </li>
      <li class="nav-item">
        <a class="nav-link th-head" id="regis-tab" data-toggle="tab" href="#registab" role="tab" aria-controls="regis" aria-selected="false">สร้างบัญชีใหม่</a>
      </li>
    </ul>

    <div class="tab-content th-head">
      <div class="tab-pane active" id="logintab" role="tabpanel" aria-labelledby="login-tab">
        <!-- ไว้ใส่โค้ดล็อคอินเฟซบุ๊ค -->
        <div class="d-flex justify-content-center">
        <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" scope="public_profile,email" onlogin="checkLoginState();" auto-logout-link="true"></div>
      </div>
        <!-- <a href="#" class="btn btn-primary btn-block">Log in with Facebook</a> -->
        <hr class="hr-text" data-content="หรือเชื่อมต่อด้วย">
        <form action="loginprocess.php" method="POST">
          <div class="form-group">
            <label for="email">อีเมล</label>
            <input type="text" name="email" id="email" class="form-control" placeholder="Enter email">
          </div>
          <div class="form-group">
            <label for="password">รหัสผ่าน</label><a href="#" class="float-right">ลืมรหัสผ่าน?</a>
            <input type="password" name="password" id="password" class="form-control" placeholder="Enter password">
          </div>
          <button type="submit" class="btn btn-success btn-block">เข้าสู่ระบบ</button>
        </form>
      </div>
      <div class="tab-pane" id="registab" role="tabpanel" aria-labelledby="regis-tab">

        <form action="register.php" method="post">
          <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
          </div>

         <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="fname">ชื่อ</label>
                <input type="text" class="form-control" id="fname"  name="fname" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="lname">สกุล</label>
                <input type="text" class="form-control" id="lname" name="lname" required>
              </div>
            </div>
         </div>

         <div class="form-group">
            <label for="tell">เบอร์โทรศัพท์</label>
            <input type="text" class="form-control" id="tell" name="tell" required>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="pass">รหัสผ่าน</label>
                <input type="password" class="form-control" id="pass" name="pass" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="re-pass">ยืนยันรหัสผ่าน</label>
                <input type="password" onblur="validate()" class="form-control" id="re-pass" >
              </div>
            </div>
         </div>

         <button type="submit" name="submit" class="btn btn-outline-success btn-block"> ยืนยันข้อมูล </button>

        </form>

      </div>
    </div>


</div>
<div class="modal-footer">

  <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary">Save changes</button> -->
</div>
</div>
</div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="facebook.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>