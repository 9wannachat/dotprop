<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dotprop</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <style>
      .fix_line{
      
        line-height:1.2em;
        height:3.6em;
        overflow:hidden;
      
      }
    </style>
</head>
<body>
    <?php          
        include 'header.php'; 
    ?>
    <main role="main">
        <div class="album py-5">
        <div class="container">
            <div class="row">
            <?php
            $getNews = "SELECT * FROM dotprop_news";
            $resNews = $conn->query($getNews);
            while($res_News = $resNews->fetch_assoc()){                
                ?>
                <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <img class="card-img-top" src="admin/newimg/<?php echo $res_News['news_img']; ?>" alt="Card image cap">
                    <div class="card-body">
                    <h5 class="card-title"><?php echo $res_News['news_name']; ?></h5>
                    <p class="card-text fix_line"><?php echo $res_News['news_short']; ?></p>
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="viewnews.php?id=<?php echo $res_News['news_id']; ?>">อ่านต่อ >>></a>
                    </div>
                    </div>
                </div>
                </div>
                <?php
            }
            ?>            
            </div>
        </div>
    </main>

    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js" charset="utf-8"></script>
    
</body>
</html>