
<?php
  include 'condb/condb.php';
  
  $viewnews = "SELECT * FROM dotprop_news";
  $res_news = $conn->query($viewnews);
?>
<!--
***************************************************************
                  ## เปลี่ยนสีแถบเมนู ##
สีแถบเมนูแก้ที่คลาสของแท็ก nav
- bg-dark (สีดำ)
- bg-light (สีเทา)
- bg-success (สีเขียว)
- bg-warning (สีเหลือง)
- bg-primary (สีฟ้า)
- bg-danger (สีแดง)
- bg-info (สีฟ้าเขียว)
- bg-white (สีขาว)
หากเปลี่ยนสีแถบเมนูต้องเปลี่ยนคลาสสีตัวอักษรด้วย
- navbar-dark (สำหรับพื้นหลังสีเข้ม ตัวอักษรจะเป็นสีขาว)
- navbar-light (สำหรับพื้นหลังสีอ่อน ตัวอักษรจะเป็นสีดำ)

                  ## เปลี่ยนสี Logo ##
แก้โค้ดสีในแท็ก span ที่ครอบคำว่า Dotprop กับ Hatyai ได้เลย

***************************************************************
-->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-success">
  <!-- Banner -->

<a class="navbar-brand logo" href="index.php" style="font-weight: 600;"><span style="color: #CCCCFF;">Dotprop</span><span style="color: #FFFFFF;">Hatyai</span></a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<!-- End banner -->
<!-- menubar -->

<div class="collapse navbar-collapse" id="navbarNavDropdown">
<ul class="navbar-nav">

<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" id="dd1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    อสังหาริมทรัพย์
  </a>
  <div class="dropdown-menu mega-menu" aria-labelledby="dd1">
    <div class="row">
      <div class="col-md-6">
        <p><strong class="sub-menu-heading th-head">เช่า</strong></p><hr>
        <a class="dropdown-item" href="viewpost.php?op=002&tp=1">บ้าน</a>
        <a class="dropdown-item" href="viewpost.php?op=002&tp=2">คอนโด</a>
        <a class="dropdown-item" href="viewpost.php?op=002&tp=3">ที่ดิน</a>
      </div>
      <div class="col-md-6">
        <p><strong class="sub-menu-heading th-head">ขาย</strong></p><hr>
        <a class="dropdown-item" href="viewpost.php?op=001&tp=1">บ้าน</a>
        <a class="dropdown-item" href="viewpost.php?op=001&tp=2">คอนโด</a>
        <a class="dropdown-item" href="viewpost.php?op=001&tp=3">ที่ดิน</a>
      </div>
    </div>
  </div>
</li>

<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" id="dd2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    จังหวัด
  </a>
  <div class="dropdown-menu mega-menu-2" aria-labelledby="dd2">
    <div class="row">
      <div class="col-md-2">
        <p><strong class="sub-menu-heading th-head">ภาคเหนือ</strong></p><hr>
        <?php
          $getNorth = "SELECT * FROM province where GEO_ID = 1";
          $resNorth = $conn->query($getNorth);
          while($north = $resNorth->fetch_assoc()){
            ?>
               <a class="dropdown-item" href="viewpost.php?id=<?php echo $north['PROVINCE_ID'] ?>"><?php echo $north['PROVINCE_NAME'] ?></a>
            <?php
          }
        ?>
        
      </div>
      <div class="col-md-2">
        <p><strong class="sub-menu-heading th-head">ภาคกลาง</strong></p><hr>
        <?php
          $getNorth = "SELECT * FROM province where GEO_ID = 2";
          $resNorth = $conn->query($getNorth);
          while($north = $resNorth->fetch_assoc()){
            ?>
               <a class="dropdown-item" href="viewpost.php?id=<?php echo $north['PROVINCE_ID'] ?>"><?php echo $north['PROVINCE_NAME'] ?></a>
            <?php
          }
        ?>
      </div>
      <div class="col-md-2">
        <p><strong class="sub-menu-heading th-head">ภาคตะวันออกเฉียงเหนือ</strong></p><hr>
        <?php
          $getNorth = "SELECT * FROM province where GEO_ID = 3";
          $resNorth = $conn->query($getNorth);
          while($north = $resNorth->fetch_assoc()){
            ?>
               <a class="dropdown-item" href="viewpost.php?id=<?php echo $north['PROVINCE_ID'] ?>"><?php echo $north['PROVINCE_NAME'] ?></a>
            <?php
          }
        ?>
      </div>
      <div class="col-md-2">
        <p><strong class="sub-menu-heading th-head">ภาคตะวันตก</strong></p><hr>
        <?php
          $getNorth = "SELECT * FROM province where GEO_ID = 4";
          $resNorth = $conn->query($getNorth);
          while($north = $resNorth->fetch_assoc()){
            ?>
               <a class="dropdown-item" href="viewpost.php?id=<?php echo $north['PROVINCE_ID'] ?>"><?php echo $north['PROVINCE_NAME'] ?></a>
            <?php
          }
        ?>
      </div>
      <div class="col-md-2">
        <p><strong class="sub-menu-heading th-head">ภาคตะวันออก</strong></p><hr>
        <?php
          $getNorth = "SELECT * FROM province where GEO_ID = 5";
          $resNorth = $conn->query($getNorth);
          while($north = $resNorth->fetch_assoc()){
            ?>
               <a class="dropdown-item" href="viewpost.php?id=<?php echo $north['PROVINCE_ID'] ?>"><?php echo $north['PROVINCE_NAME'] ?></a>
            <?php
          }
        ?>
      </div>
      <div class="col-md-2">
        <p><strong class="sub-menu-heading th-head">ภาคใต้</strong></p><hr>
        <?php
          $getNorth = "SELECT * FROM province where GEO_ID = 6";
          $resNorth = $conn->query($getNorth);
          while($north = $resNorth->fetch_assoc()){
            ?>
              <a class="dropdown-item" href="viewpost.php?id=<?php echo $north['PROVINCE_ID'] ?>"><?php echo $north['PROVINCE_NAME'] ?></a>
            <?php
          }
        ?>
      </div>
    </div>
  </div>
</li>

<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" id="dd3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    เท่าทันอสังหา
  </a>
  <div class="dropdown-menu" aria-labelledby="dd3">
    <?php
      while($resNews = $res_news -> fetch_assoc()){
        ?>
         <a class="dropdown-item" href="viewnews.php?id=<?php echo $resNews['news_id']; ?>"><?php echo $resNews['news_name']; ?></a>
        <?php
      }
    ?>
   
    
  </div>
</li>

<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#" id="dd4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    ลงประกาศ
  </a>
  <div class="dropdown-menu" aria-labelledby="dd4">
    <a class="dropdown-item" href="announce.php">กระกาศขาย</a>
    <a class="dropdown-item" href="announce.php">ประกาศเช่า</a>
    <a class="dropdown-item" href="#">ขั้นตอนการลงประกาศ</a>
  </div>
</li>

<li class="nav-item">
  <a class="nav-link" href="map.php">ค้นหาด้วยแผนที่</a>
</li>
</ul>

</div>
<div class="navbar-collapse collapse w-50 order-3 dual-collapse2">
<ul class="navbar-nav ml-auto">
    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#searchbar"><i class="fas fa-search"></i></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#login"><i class="fas fa-lock"></i></a>
    </li>
</ul>
</div>
</nav>
<!-- End menubar -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=5e447899-5ab0-45cb-a2d8-08f0ec1ec90c"> </script>

<?php include 'modal-search.php'; ?>

<!--
*********************************************************
            ## ส่วนสมัครสมาชิก ##
*ไม่แน่ใจว่าใช้ข้อมูลอะไรสมัครสมาชิกบ้างแต่สามารถนำตัวอย่างฟอร์ม
จากส่วน ค้นหา กับ เข้าสู่ระบบ มาเป็นแนวทางการเขียนได้เลย

*********************************************************
-->
<?php include 'modal-login.php'; ?>

<!-- <?php include 'floatButton.html'?> -->

