<?php
  include 'condb/condb.php';

  $sqlGetMarker = "SELECT prop_id,prop_lat,prop_lng,prop_price FROM properties inner join proppost on proppost.post_prop = properties.prop_id where post_verify = 1 "; 

  $resultGetMarker = $conn->query($sqlGetMarker);

  $i=0;
  $arr= array();

  if($resultGetMarker->num_rows > 0){
    while($resultMarker = $resultGetMarker->fetch_assoc()){
      $i++;
      $latLng = array();

      $latLng['id'] = $resultMarker["prop_id"];
      $latLng['price'] = $resultMarker["prop_price"];
      $latLng['lat'] = $resultMarker["prop_lat"];
      $latLng['lng'] = $resultMarker["prop_lng"];

      $arr[$i] = $latLng;

    }
  }
   $JSONval = json_encode($arr);
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Dotprop</title>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="css/dotprop.css">
  <link rel="stylesheet" href="css/all.css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/carousel.css">
  <link rel="stylesheet" href="css/megamenu.css">
  <link rel="stylesheet" href="css/modalsb.css">
  <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>

<body>
  <header>

    <?php include 'header.php'; ?>

  </header>

  <main role="main">
    <div class="container py-4">
      <h2 class="th-head">
        <div class="row">
          <div class="col-md-3">
            <span style="color:green;">| ค้นหา</span>ด้วยแผนที่
          </div>
          <div class="col-md-9">
            <div class="input-group">

              <input type="text" class="form-control" id="search" placeholder="ค้นหาสถานที่"
                aria-describedby="spanSerach" required>
              <div class="input-group-prepend">
                <span class="input-group-text" id="spanSerach" onclick="search();">ค้นหา</span>
              </div>
            </div>

          </div>

      </h2>
      <div id="googleMap" style="width:100%;height:600px;"></div>
    </div>
  </main>

  <!-- footer Zone -->

  <!-- end footer zone -->
  <script>

    var dataJson = <?php echo $JSONval; ?>

      function myMap() {
        var count = Object.keys(dataJson).length

        var mapProp = {
          center: new google.maps.LatLng(13.7649, 100.5383),
          zoom: 6,
        }

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp)

        for (let i = 1; i <= count; i++) {
          var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: { lat: parseFloat(dataJson[i].lat), lng: parseFloat(dataJson[i].lng) }
          })

          marker.addListener('click', function (event) {
            // alert("Latitude: " + event.latLng.lat() + " " + ", longitude: " + event.latLng.lng())
            window.location = "https://localhost/dotprop/viewdetail.php?id="+dataJson[i].id
          })

          marker.setMap(map)
        }
      }

    function search() {
      var query = document.getElementById('search').value
      console.log(query)

      $.ajax({
        url: "searchMaps.php",
        method: "POST",
        data: { query: query },
        success: function (data) {
          var objData = JSON.parse(data)
          var size = Object.keys(objData).length
          console.log(objData)
          drawMarkerSearch(size,objData)
        }
      })

    }

    function drawMarkerSearch(size,objData) {
      var mapProp = {
        center: new google.maps.LatLng(objData[1].lat, objData[1].lng),
        zoom: 9,
      }

      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp)

      for (let i = 1; i <= size; i++) {
          var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: { lat: parseFloat(objData[i].lat), lng: parseFloat(objData[i].lng) }
          })

          marker.addListener('click', function (event) {
            // alert("Latitude: " + event.latLng.lat() + " " + ", longitude: " + event.latLng.lng() + "======>"+objData[i].lat)
            window.location = "https://localhost/dotprop/viewdetail.php?id="+objData[i].id
          })

          marker.setMap(map)
        }


    }

  </script>

  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
  <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2pDj3lbScNYKRBsO6K1QcTc24OASU5J4&callback=myMap"></script>
</body>

</html>