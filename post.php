<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dotprop</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">

  </head>
  <body>
    <header>


    </header>

<main role="main">
  <div class="container my-4">
    <div class="row">
      <div class="col-md-9 col-sm-12">
        <!-- post -->
        <div class="card">
          <div class="card-body px-4">

            <div class="row">
              <h4 class="col-md-8 card-title sarabun w-75">หัวข้อรายการสินทรัพย์..</h4>
              <div class="col-md-4 text-right">
                <h4 class="card-title sarabun">THB 6,800,000</h4>
              </div>
            </div>
            <div class="row my-2">
              <div class="col-md-4">
                <h6 class="card-subtitle mb-2 sarabun">
                  <i class="fas fa-map-marker-alt"></i>&nbsp; หาดใหญ่, สงขลา
                </h6>
              </div>
              <div class="col-md-8">
                <p class="card-subtitle sarabun text-right">
                  อัปเดทล่าสุดเมื่อ..............
                </p>
              </div>
            </div>

            <hr>

            <!-- Gallery -->
            <div class="slider slider-for text-center">
				<div>
					<img src="img/user2.jpg" alt="" width="100%">
				</div>
				<div>
					<img src="img/user.png" alt="" >
				</div>
				<div>
					<img src="img/user2.jpg" alt="" width="100%">
				</div>
				<div>
					<img src="img/user.png" alt="">
				</div>
				<div>
					<img src="img/user2.jpg" alt="" width="100%">
				</div>
			</div>
			<div class="mt-2 slider slider-nav text-center">
				<div>
					<img src="img/user2.jpg" alt="" width="200px">
				</div>
				<div>
					<img src="img/user.png" alt="" >
				</div>
				<div>
					<img src="img/user2.jpg" alt="" width="200px">
				</div>
				<div>
					<img src="img/user.png" alt="">
				</div>
				<div>
					<img src="img/user2.jpg" alt="" width="200px">
				</div>
			</div>
            <!-- End Gallery -->
            <h6 class="lead sarabun py-2">| รายละเอียดทรัพย์</h6>
            <p class="card-text">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae nostrum facere dicta, sapiente recusandae labore provident. Quisquam, rerum aperiam reiciendis placeat commodi totam maiores praesentium delectus saepe perspiciatis architecto modi.
            </p>
            <h6 class="lead sarabun py-2">| แผนที่</h6>
            <div>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.042053404786!2d100.45907781532603!3d7.004331019328515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x304d2869cefed5df%3A0x6bbf3042956b925!2z4Lie4Lil4Lix4Liq4LiE4Lit4LiZ4LmC4LiUIOC4q-C4suC4lOC5g-C4q-C4jeC5iDI!5e0!3m2!1sth!2sth!4v1549570777498" width="770" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

          </div>
        </div>
        <!-- end post -->
      </div>
      <div class="col-md-3 col-sm-12">

        <div class="card">
          <div class="card-header">
            <div class="text-center sarabun">
              <h5>ผู้ประกาศ</h5>
            </div>
          </div>
          <div class="card-body">
            <img src="" alt="" class="img-circle">
            <div class="text-center sarabun">
              <img src="img/user2.jpg" class="rounded-circle mx-auto d-block" alt="..." width="100px" height="100"><br>
              <h6>Username</h6>
              <small>email</small>
            </div>
          </div>
        </div>

        <div class="card my-3">
          <div class="card-body">
            Facebook Page Script
          </div>
        </div>

      </div>
    </div>
  </div>
</main>

<!-- footer Zone -->

<!-- end footer zone -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.min.js" charset="utf-8"></script>
<script src="slick/slick.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // $('.your-class').slick({
    //   setting-name: setting-value
    // });
    $('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  // arrows: true,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: false,
  centerMode: true,
  focusOnSelect: true
});
  });
</script>
</body>
</html>
