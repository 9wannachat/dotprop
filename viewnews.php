<?php
    include 'condb/condb.php';
    if(isset($_GET['id'])){
        $id=$_GET['id'];
        $viewnews = "SELECT * FROM dotprop_news where news_id = '$id'";
        $res_news = $conn->query($viewnews); 
        $res = $res_news->fetch_assoc();         
    }    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>dotprop|<?php echo $res['news_name'] ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
</head>
<body>
<header>
    <?php
          
            include 'header.php'; 
    ?>
</header>
<main role="main">
    <div class="album py-5">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h4><?php echo $res['news_name']; ?></h4>
            </div>
            <div class="card-body">
                <div>
                    <?php echo $res['news_detail']; ?>
                </div>
            </div>
            <div class="card-footer">
                <div class="float-right">
                    <p>อัพเดทล่าสุด : <?php echo $res['new_time']; ?></p>
                </div>                
            </div>
        </div><br>
        <div class="d-flex justify-content-center">
            <a href="index.php" class="btn btn-outline-info">ย้อนกลับ</a>
        </div>
    </div>
    </div>
</main>
   
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.min.js" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>