<?php
    include 'condb/condb.php';
    if(isset($_POST['query'])){

        $datas = array();

        $search = $_POST['query'];
        $i=0;

        $sqlProvince = "SELECT * FROM province inner join properties on properties.prop_province = province.PROVINCE_ID where  PROVINCE_NAME LIKE '%".$search."%'";
        $resProvince = $conn->query($sqlProvince);


        if($resProvince ->num_rows > 0 ){
            while($row = $resProvince->fetch_assoc()){
                $i++;
                $rawData = array();

                $rawData['id'] = $row['prop_id'];
                $rawData['name'] = $row['prop_topic'];
                $rawData['lat'] = $row["prop_lat"];
                $rawData['lng'] = $row["prop_lng"];
                
               
                $datas[$i] = $rawData;                
            }

            echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        }else{
            $sqlAmphur = "SELECT * FROM amphur inner join properties on properties.prop_amphur = amphur.AMPHUR_ID where AMPHUR_NAME LIKE '%".$search."%'";
            $resAmphur = $conn->query($sqlAmphur);

            if($resAmphur ->num_rows >0){
               while($row = $resAmphur -> fetch_assoc()){
                    $i++;
                    $rawData = array();

                    $rawData['id'] = $row['prop_id'];
                    $rawData['name'] = $row['prop_topic'];
                    $rawData['lat'] = $row["prop_lat"];
                    $rawData['lng'] = $row["prop_lng"];
                    
                    $datas[$i] = $rawData;
               }

               echo json_encode($datas,JSON_UNESCAPED_UNICODE);
            }else{
                $sqlDistric = "SELECT * FROM amphur inner join properties on properties.prop_distric = distric.DIDTRIC_ID where DISTRIC_NAME LIKE '%".$search."%'";
                $resDistric = $conn->query($sqlDistric);

                if($resDistric->num_rows > 0){
                    while($row = $resDistric -> fetch_assoc()){
                        $i++;
                        $rawData = array();
    
                        $rawData['id'] = $row['prop_id'];
                        $rawData['name'] = $row['prop_topic'];
                        $rawData['lat'] = $row["prop_lat"];
                        $rawData['lng'] = $row["prop_lng"];
                        
                       
                        $datas[$i] = $rawData;
                    }
                }else{
                    $datas['status'] = "No Data";
                }

                echo json_encode($datas,JSON_UNESCAPED_UNICODE);
        }
        
    }
}
?>