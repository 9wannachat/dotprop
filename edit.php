<?php
    include 'condb/condb.php';
    session_start();

    $cus_id = $_SESSION['cus_id'];
    $data = "SELECT * FROM member where mem_id = '$cus_id'";
    $result = $conn->query($data);
    if($result->num_rows>0){
        while($res = $result->fetch_assoc()){
            $name = $res['mem_name'];
            $mail = $res['mem_mail'];
            $img = $res['mem_pic'];
            $tel = $res['mem_tel'];
        }
    }

    $ex_name = explode(" ",$name);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dotprop</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h3 class="th-head text-center" >แก้ไขข้อมูลส่วนตัว</h3>
            </div>
            <div class="card-body">
                <form action="editprocess.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="cus_id" value="<?php  echo $cus_id; ?>">
                    <div class="text-center sarabun">
                            <img src="imgprofile/<?php echo $img; ?>" id="blah" class="rounded-circle mx-auto d-block" alt="..." width="150px" height="150px"><br>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="custom-file col-md-4">
                            <input type="file" class="custom-file-input" name="file" id="imgInp">
                            <label class="custom-file-label" for="imgInp">Choose file</label>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <label for="fname">ชื่อ</label>
                            <input type="text" class="form-control" id="fname" name="fname" value="<?php echo $ex_name[0]; ?>" >
                        </div>
                        <div class="col-md-6">
                            <label for="lname">สกุล</label>
                            <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $ex_name[1]; ?>" >
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <label for="mail">Email</label>
                            <input type="text" class="form-control" id="mail"  name="mail" value="<?php echo $mail; ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="tel">เบอร์โทรศัพท์</label>
                            <input type="text" class="form-control" id="tel" name="tel" value="<?php echo $tel; ?>" >
                        </div>
                    </div><br>
                    <div class="d-flex justify-content-center">
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-outline-success" type="submit" name="submit">ยืนยัน</button>
                            </div>
                            <div class="col-md-8">
                                <a href="profile.php" class="btn btn-outline-danger" >ย้อนกลับ</a>
                            </div>
                        </div>
                    </div>
               </form>
            </div>
         </div>
    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
</body>
</html>