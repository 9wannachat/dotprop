<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dotprop</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">

  </head>
  <body>
    <header>

</header>



<main role="main">

  <!--
  *********************************************************
                  ## ส่วนภาพสไลด์ ##
      สามารถเปลี่ยนภาพได้ที่แท็ก img เลย
      * หากไม่ต้องการข้อความและปุ่ม สามารถลบออกหรือคอมเมนต์ไว้
    เผื่อในอนาคตอยากใช้ได้ตามตัวอย่างรูปสไลด์ที่ 3

  *********************************************************
   -->

      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="img/banner1.jpg" alt="First slide">
            <!-- <div class="container">
              <div class="carousel-caption text-left">
                <h1>Example headline.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
              </div>
            </div> -->
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="img/banner2.jpg" alt="Second slide">
            <!-- <div class="container">
              <div class="carousel-caption">
                <h1>Another example headline.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
              </div>
            </div> -->
          </div>
          <div class="carousel-item">
            <img class="third-slide" src="img/banner3.jpg" alt="Third slide">
            <!-- <div class="container">
              <div class="carousel-caption text-right">
                <h1>One more for good measure.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
              </div>
            </div> -->
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      <div class="container">
        <div class="row">
          <h1 class="th-head w-50"><span style="color:green;">| เช่า</span>บ้านทั้งหมด</h1>
          <div class="float-right" style="padding-top: 10px;">
            <h3 class="th-head"><span style="color:green;">> </span>จังหวัดสงขลา</h3>
          </div>
        </div>
      </div>
      <div class="album py-5">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">ขาย</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">2.2 ลบ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">ขาย</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">2.2 ลบ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">ขาย</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">2.2 ลบ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">เช่า</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">9,000 บ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">เช่า</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">9,000 บ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">เช่า</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">9,000 บ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <h1 class="th-head w-50"><span style="color:green;">| บ้าน</span>ทั้งหมด</h1>
          <div class="float-right" style="padding-top: 10px;">
            <h3 class="th-head"><span style="color:green;">> </span>จังหวัดเชียงราย</h3>
          </div>
        </div>
      </div>
      <div class="album py-5">
        <div class="container">

          <div class="row">
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">ขาย</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">2.2 ลบ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">ขาย</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">2.2 ลบ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <h5 class="card-title">Plus Condo 2</h5>
                  <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">ขาย</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-center">
                        <a href="#" class="th-head btn btn-light btn-block">2.2 ลบ.</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


  </main>

  <!--
  *********************************************************
                  ## ส่วนท้าย ##
      ทำการแบ่งคอลัมน์ไว้ให้ 3 คอลัมน์เผื่อๆ เปลี่ยนเนื้อหาได้เลย

      * ส่วนไอคอนโซเชียลด้านล่าง จะเอาออกก็ได้แล้วแต่เลยครับ

  *********************************************************
   -->

   <!-- footer Zone -->

   <!-- end footer zone -->

  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/holder.min.js" charset="utf-8"></script>
  </body>
</html>
