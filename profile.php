<?php
  include 'condb/condb.php';
  session_start();
  if($_SESSION['cus_id']==null){
    header("Location:login.php");
  }
  $cus_id = $_SESSION['cus_id'];

  $data = "SELECT * FROM member where mem_id = '$cus_id'";
  $result = $conn->query($data);
  if($result->num_rows>0){
    while($res = $result->fetch_assoc()){
      $name = $res['mem_name'];
      $mail = $res['mem_mail'];
      $img = $res['mem_pic'];
    }
  }

  $getPost = "SELECT * FROM proppost WHERE post_member = '$cus_id' AND post_status = 001 ";
  $resPost = $conn->query($getPost);
  $countPost = $resPost->num_rows;
  
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dotprop</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
  </head>
  <body>
<header>
  <?php include 'header.php'; ?>
</header>

<main role="main">
  <div class="container my-4">

    <div class="row">
      <div class="col-md-9 col-sm-12">
        <div class="card px-3 my-2">
          <div class="card-body">
            <div class="row">
              <div class="col-md-9">
                <h3 class="th-head"><i class="fas fa-bullhorn"></i> ประกาศของฉัน ( <?php echo $countPost; ?> ประกาศ )</h3>
              </div>
              <div class="col-md-3">
                <a href="announce.php" class="btn btn-dark th-head btn-block">ลงประกาศใหม่</a>
              </div>
            </div>
          </div>

        </div>
        <!-- post -->
      <?php
      
      while($resPostdb = $resPost->fetch_assoc()){
       
        $prop_post = $resPostdb['post_prop'];
        $getPostDetail = "SELECT * FROM properties  inner join province on province.PROVINCE_ID = properties.prop_province
                                                    inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                                    inner join proppost on properties.prop_id = proppost.post_prop
                                                    inner join status on status.status_id = proppost.post_verify
                                                    inner join operation on operation.op_id = properties.prop_oper
                                                    inner join proptype on proptype.type_id = properties.prop_type  where prop_id = '$prop_post'
                                                    -- inner join propimage on propimage.img_prop = properties.prop_id where prop_id = '$prop_post'";
        $resPostDetail = $conn->query($getPostDetail);
        while($postDetail = $resPostDetail->fetch_assoc()){
                        $propId =  $postDetail['prop_id'];                    
                        $getImg = "SELECT * FROM propimage WHERE img_prop = $propId ORDER BY img_id DESC LIMIT 1";
                        $resImg = $conn->query($getImg);
                        $Img = $resImg->fetch_assoc();
          ?>
           <div class="card my-2">
            <div class="row no-gutters">
                <div class="col-5">
                    <img src="upload/<?php echo $Img['img_name']; ?>"  alt="" width="300px" height="300px">
                </div>
                <div class="col-7">
                    <div class="card-block px-2 py-2">
                        <h4 class="card-title th-head"><?php echo $postDetail['prop_topic']; ?><strong class="text-danger">&nbsp;[<?php echo $postDetail['status_name']; ?>]</strong></h4>
                        <h4 class="card-title"><?php echo $postDetail['prop_price']; ?> ฿</h4>
                        <div class="row">
                          <a href="#" class="btn btn-primary sarabun"><?php echo $postDetail['op_name']; ?></a>
                          <a href="#" class="badge badge-secondary"><br><?php echo $postDetail['type_name']; ?></a>
                          <!-- <button onclick="clicked()"  class="btn btn-light sarabun"><i class="fas fa-cog"></i> จัดการโพสต์</button> -->
                          <a href="manage.php?id=<?php echo $postDetail['prop_id']; ?>" class="btn btn-light sarabun"><i class="fas fa-cog"></i> จัดการโพสต์</a>
                        </div>
                        <h6 class="card-subtitle mb-2 sarabun my-3">
                          <i class="fas fa-map-marker-alt"></i>&nbsp; <?php echo $postDetail['AMPHUR_NAME']; ?>,<?php echo $postDetail['PROVINCE_NAME']; ?>
                        </h6>
                        <p class="card-text"><?php echo $postDetail['prop_detail']; ?></p>
                        <table class="table table-bordered text-center sarabun">
                          <tr>
                            <td><i class="fas fa-ruler-combined"></i> <?php echo $postDetail['prop_space']; ?> ตร.ม.</td>
                            <!-- <td><i class="fas fa-ruler-combined"></i> - ตร.ม.</td> -->
                            <td><i class="fas fa-bed"></i> <?php echo $postDetail['prop_bedroom']; ?> ห้องนอน</td>
                            <td><i class="fas fa-bath"></i> <?php echo $postDetail['prop_bathroom']; ?> ห้องน้ำ</td>
                          </tr>
                        </table>
                    </div>
                </div>
            </div>
            </div>
            <script>
              function clicked(){
              var ck = confirm("ต้องการลบประกาศขายนี้จริงหรือ");
                if(ck==true){
                  window.location.href = 'http://localhost/dotprop/deletepost.php?id=<?php echo $postDetail['prop_id'] ; ?>';
                }else{
                  return;
                }
              }
          </script>    
          <?php
        }
      }
      ?>
       

        <!-- end post -->
      </div>
      <div class="col-md-3 col-sm-12">

        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="th-head pl-2">
                <h5>โปรไฟล์</h5>
              </div>
              <div class="sarabun ml-auto pr-2">
                <a href="edit.php" class="text-secondary"><i class="fas fa-user-edit"></i></a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <img src="" alt="" class="img-circle">
            <div class="text-center sarabun">
              <img src="imgprofile/<?php echo $img; ?>" class="rounded-circle mx-auto d-block" alt="..." width="100px" height="100px"><br>
              <h6> <?php echo $name; ?> </h6>
              <small> <?php echo $mail; ?> </small>
              <a href="logout.php" class="btn btn-outline-success btn-block">Logout</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</main>

<!-- footer Zone -->

<!-- end footer zone -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.min.js" charset="utf-8"></script>
</body>
</html>
