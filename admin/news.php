<?php
    include '../condb/condb.php';
    $getNews = "SELECT * FROM dotprop_news";
    $resNews = $conn->query($getNews);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dotprop</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
   
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <style>
    .responsive {
        text-overflow: ellipsis;
        overflow-x:auto;
    }
  </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    DOTPROP
                </a>
            </div>

            <ul class="nav">
                <!-- <li>
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li> -->
                <li>
                    <a href="#">
                        <i class="pe-7s-news-paper"></i>
                        <p>Post</p>
                    </a>
                    <li>
                        <a href="memberpost.php">
                            <p>Nonverify</p>
                        </a>
                    </li>
                    <li>
                        <a href="postaccept.php">
                            <p>Verify</p>
                        </a>
                    </li>
                </li>
                <li>
                    <a href="member.php">
                        <i class="pe-7s-users"></i>
                        <p>Member</p>
                    </a>
                </li>
                <li class="active">
                    <a href="news.php">
                        <i class="pe-7s-ribbon"></i>
                        <p>NEWS</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">NEWS</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">                             
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li>
                           <a href="">
                               <p>Account</p>
                            </a>
                        </li> -->
           
                        <li>
                            <a href="logout.php">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="header">
                                <h4 class="title">ข่าวสาร</h4>
                                <hr>
                            </div>
                            <div class="content">

                           <div class="row">
                           <?php
                                while($res = $resNews->fetch_assoc()){                            
                            ?>
                                 <div class="col-md-6">
                                    <div class="card">
                                        <div class="header">
                                            <h4 class="title"><?php echo $res['news_name']; ?></h4>
                                            <hr>
                                        </div>
                                        <div class="content responsive">
                                            <?php echo $res['news_detail']; ?>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="editnews.php?id=<?php echo $res['news_id']; ?>" class="btn btn-block btn-warning">แก้ไขข่าว</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>     
                            <?php
                                }
                           ?>
                                                   
                          
                                <!-- <div class="col-md-6">
                                   
                                    <div class="card">
                                        <div class="header">
                                            <h4 class="title">มีทรัพย์ก็ลงทุนได้</h4>
                                            <hr>
                                        </div>
                                        <div class="content">

                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#" class="btn btn-block btn-warning">แก้ไขข่าว</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div> -->
                           </div>
                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">

        </footer>

    </div>
</div>


</body>


    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>

</html>
