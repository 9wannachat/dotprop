<?php
    include '../condb/condb.php';

    $getMember = "SELECT * FROM member";
    $resMember = $conn->query($getMember);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dotprop</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    DOTPROP
                </a>
            </div>

            <ul class="nav">
                <!-- <li >
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li> -->
                <li>
                    <a href="#">
                        <i class="pe-7s-news-paper"></i>
                        <p>Post</p>
                    </a>
                    <li>
                        <a href="memberpost.php">
                            <p>Nonverify</p>
                        </a>
                    </li>
                    <li>
                        <a href="postaccept.php">
                            <p>Verify</p>
                        </a>
                    </li>
                </li>
                <li class="active">
                    <a href="member.php">
                        <i class="pe-7s-users"></i>
                        <p>Member</p>
                    </a>
                </li>
                <li>
                    <a href="news.php">
                        <i class="pe-7s-ribbon"></i>
                        <p>NEWS</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Member</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">                             
								<p class="hidden-lg hidden-md">Member</p>
                            </a>
                        </li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li>
                           <a href="">
                               <p>Account</p>
                            </a>
                        </li> -->
           
                        <li>
                            <a href="logout.php">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="header">
                                <h4 class="title">Member Data Table</h4>
                               
                            </div>
                            <div class="content">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>รหัส</th>
                                            <th>รูปโปรไฟล์</th>
                                            <th>ชื่อ</th>
                                            <th>เบอร์โทรศัพท์</th>
                                            <th>email</th>
                                            <th>facebook</th>
                                            <th>setting</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            while($res = $resMember->fetch_assoc()){
                                        ?>
                                          <tr>
                                            <td><?php echo $res['mem_id']; ?></td>
                                            <td><img src="../imgprofile/<?php echo $res['mem_pic']; ?>" class="img-circle" width="100px" hieght="100px" ></td>
                                            <td><?php echo $res['mem_name']; ?></td>
                                            <td><?php echo $res['mem_tel']; ?></td>
                                            <td><?php echo $res['mem_mail']; ?></td>
                                            <td><?php echo $res['mem_facebook']; ?></td>
                                            <td width="150px">
                                                <a href="editprofile.php?id=<?php echo $res['mem_id']; ?>" class="btn btn-warning">แก้ไข</a>
                                                <button  onclick="clicked()" class="btn btn-danger">ลบ</button>
                                            </td>
                                        </tr>
                                        <script>
                                            function clicked(){
                                            var ck = confirm("ต้องการลบประกาศขายนี้จริงหรือ");
                                                if(ck==true){
                                                window.location.href = 'http://localhost/dotprop/admin/deletemember.php?id=<?php echo $res['mem_id'] ; ?>';
                                                }else{
                                                return;
                                                }
                                            }
                                        </script>   
                                        <?php
                                            }
                                        ?>
                                      
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">

        </footer>

    </div>
</div>



</body>


    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>


</html>
