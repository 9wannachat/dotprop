<?php
    include '../condb/condb.php';
    if(isset($_POST['submit'])){
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $tel = $_POST['tel'];
        $cus_id = $_POST['id'];

        $fullname = $fname." ".$lname;
        
        $file = $_FILES['file'];
        $fileName = $file['name'];
        $filTmpName = $file['tmp_name'];
        $fileSize = $file['size'];
        $fileError = $file['error'];
        $fileType = $file['name'];

        if($fileError == 0){
            $fileExt = explode('.', $fileName);
            $fileActualExt = strtolower(end($fileExt));
            $allowed = array("jpeg","jpg","png");
        
            date_default_timezone_set("Asia/Bangkok");
            $now = new DateTime();
            $time = ($now->format('dmYHis'));
            
            if (in_array($fileActualExt, $allowed)) {
                if ($fileError === 0) {
                    if ($fileSize < 200000000) {
                        $fileNameNew = $time . "." . $fileActualExt;
                        $fileDestination = '../imgprofile/' . $fileNameNew;
                        move_uploaded_file($filTmpName, $fileDestination);
                        $update = "UPDATE member SET mem_name = '$fullname',mem_tel = '$tel',mem_pic = '$fileNameNew' WHERE mem_id = '$cus_id' ";   
                        
                        if($conn->query($update)==TRUE){
                            echo '<script>alert("บันทึกการเปลี่ยนแปลงสำเร็จ");</script>';
                            header("Refresh:0,url=member.php"); 
                        }else{
                            echo '<script> alert("เกิดข้อผิดพลาด");</script>';  
                            header("Refresh:0,url=member.php");                       
                        }
                 
                    } else {
                        echo '<script>alert("Image Too Big");</script>'; 
                        header("Refresh:0,url=member.php"); 
                    }
                } else {
                    echo '<script>alert("Upload Image Fail");</script>';
                    header("Refresh:0,url=member.php"); 
                }
            } else {
                echo '<script>alert("jpeg,jpg,png Only");</script>';
                header("Refresh:0,url=member.php"); 
            }

        }else{
            $update = "UPDATE member SET mem_name = '$fullname',mem_tel = '$tel' WHERE mem_id = '$cus_id' ";
            if($conn->query($update)==TRUE){
                echo '<script>alert("บันทึกการเปลี่ยนแปลงสำเร็จ");</script>';
                header("Refresh:0,url=member.php"); 
            }else{
                echo '<script> alert("เกิดข้อผิดพลาด");</script>';  
                header("Refresh:0,url=member.php");                       
            } 
        }
    }

?>