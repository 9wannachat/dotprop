<?php
  include '../../condb/condb.php';
  session_start();
  ob_start();

  if(isset($_GET['id'])){
    $id = $_GET['id'];

    $edit  = "SELECT * FROM proppost  inner join properties on properties.prop_id = proppost.post_prop 
                                      inner join propimage on propimage.img_prop = proppost.post_prop where post_prop = '$id' ";
    $result = $conn->query($edit);
    while($row = $result->fetch_assoc()){
      $prop_oper = $row['prop_oper'];
      $prop_type = $row['prop_type'];
      $prop_topic = $row['prop_topic'];
      $prop_price = $row['prop_price'];
      $prop_direct = $row['prop_direct'];
      $prop_detail = $row['prop_detail'];
      $prop_province = $row['prop_province'];
      $prop_amphur = $row['prop_amphur'];
      $prop_distric = $row['prop_distric'];
      $prop_road = $row['prop_road'];
      $prop_soi = $row['prop_soi'];
      $prop_project = $row['prop_project'];
      $prop_building = $row['prop_building'];
      $prop_space = $row['prop_space'];
      $prop_bedroom = $row['prop_bedroom'];
      $prop_bathroom = $row['prop_bathroom'];
      $prop_floor= $row['prop_floor'];
      $prop_layernumber = $row['prop_layernumber'];
      $prop_highlight = $row['prop_highlight'];
      $prop_view = $row['prop_view'];
    }
  }
 
  $oper = "SELECT * FROM operation ";
  $type = "SELECT * FROM proptype ";
  $direct = "SELECT * FROM direction";
  $province = "SELECT * FROM province";
  $amphur = "SELECT * FROM amphur";
  $district = "SELECT * FROM district";

  $con_oper = $conn->query($oper);
  $con_type = $conn->query($type);
  $con_direct = $conn->query($direct);
  $con_pro = $conn->query($province);
  $con_amphur = $conn->query($amphur);
  $con_dist = $conn->query($district);

  $numberType = $con_type->num_rows;

  function generateRandomString($length = 5) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dotprop</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script >
        function load_data2(query){
            $.ajax({
                url:"../localtion.php",
                method:"POST",
                data:{query2:query},
                success:function(data){
                    $('#result2').html(data);
                    console.log(data);
                }
            });
        }    
       
       function myFunction2(){
          var amphur = document.getElementById('amphur').value;   
          load_data2(amphur)
       } 

      

        function load_data(query){
            $.ajax({
                url:"../localtion.php",
                method:"POST",
                data:{query:query},
                success:function(data){
                    $('#result').html(data);
                    console.log(data);
                }
            });
        }    

        function myFunction(){
          var province = document.getElementById('provinceS').value;  
          load_data(province);
          
       } 


     
    </script>

  </head>
  <body>
    <header>

</header>

<main role="main">
  <div class="container py-4">
    <h2 class="sarabun py-4">แบบฟอร์มกรอกรายละเอียด</h2><hr>
    <form method="POST" class="sarabun" enctype="multipart/form-data">
      <div class="form-row">
        <div class="form-group col-md-3">
          <label class="my-1" for="type">ทำรายการ</label>
          <select class="custom-select my-1" name="operation" id="type">
                     
            <?php
              while($res_oper = $con_oper->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_oper['op_id']; ?>" <?php if($res_oper['op_id'] == $prop_oper){echo "selected"; } ?> ><?php echo $res_oper['op_name']; ?></option>               
                <?php
              }
            ?>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label class="my-1" for="estate-type">ประเภททรัพย์</label>
          <select class="custom-select my-1" name="type" id="estate-type">            
            <?php           
              while($res_type = $con_type->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_type['type_id']; ?>" <?php if($res_type['type_id'] == $prop_type){echo "selected"; } ?> ><?php echo $res_type['type_name']; ?></option>               
                <?php
              }
            ?>
            <option value="<?php echo $numberType+1; ?>">อื่นๆ</option>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label class="my-1" for="estate-type">ประเภททรัพย์</label>
          <input type="text"  id="estate-type-custom" name="other" class="form-control my-1">
        </div>
      </div>

      <div class="form-group row">
        <label class="my-1 col-md-2" for="estate-type">หัวข้อประกาศ</label>
        <div class="col-md-10">
          <input type="text" name="topic" id="estate-type-custom" class="form-control my-1" value="<?php echo $prop_topic; ?>">
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-7">
          <label for="price" class="my-1">ราคา</label>
          <input type="text" name="price" id="price" class="form-control my-1" value="<?php echo $prop_price; ?>">
        </div>
        <div class="form-group col-md-5">
          <label for="direction" class="my-1">ทิศ</label>
          <select class="custom-select my-1 mr-sm-2" name="direct" id="direction">
          
            <?php
              while($res_direct = $con_direct->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_direct['direct_id']; ?>" <?php if($res_direct['direct_id'] == $prop_direct){echo "selected"; } ?> ><?php echo $res_direct['direct_name']; ?></option>               
                <?php
              }
            ?>
          </select>
        </div>
      </div>

      <label for="estate-detail" class="my-1">รายละเอียดทรัพย์</label>
      <textarea name="estate-detail" id="estate-detail" cols="30" rows="10" class="form-control my-1"  ><?php echo $prop_detail; ?></textarea>

      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="province" class="my-1">จังหวัด</label>
          <select class="custom-select my-1 mr-sm-2" name="province" id="provinceS" onchange="myFunction()">
           
            <?php
              while($res_prov = $con_pro->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_prov['PROVINCE_ID']; ?>" <?php if($res_prov['PROVINCE_ID'] == $prop_province){echo "selected"; } ?> ><?php echo $res_prov['PROVINCE_NAME']; ?></option>               
                <?php
              }
            ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="district" class="my-1">อำเภอ</label>

          <span id="result">
          <select class="custom-select my-1 mr-sm-2" name="amphur" id="district">          
            <?php
              while($res_amp = $con_amphur->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_amp['AMPHUR_ID']; ?>" <?php if($res_amp['AMPHUR_ID'] == $prop_amphur){echo "selected"; } ?> ><?php echo $res_amp['AMPHUR_NAME']; ?></option>               
                <?php
              }
            ?>
          </select>
          </span>
        </div>
        <div class="form-group col-md-4">
          <label for="sub-district" class="my-1">ตำบล</label>
          <span id="result2">
          <select class="custom-select my-1 mr-sm-2" name="district" id="sub-district">           
            <?php
              while($res_dist = $con_dist->fetch_assoc()){
                ?>
                  <option value="<?php echo $res_dist['DISTRICT_ID']; ?>" <?php if($res_dist['DISTRICT_ID'] == $prop_distric){echo "selected"; } ?> ><?php echo $res_dist['DISTRICT_NAME']; ?></option>               
                <?php
              }
            ?>
          </select>
            </span>
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-2">
          <label for="road" class="my-1">ถนน</label>
          <input type="text" name="road" id="road" class="form-control my-1" value="<?php echo $prop_road; ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="soi" class="my-1">ซอย</label>
          <input type="text" name="soi" id="soi" class="form-control my-1" value="<?php echo $prop_soi; ?>" >
        </div>
        <div class="form-group col-md-4">
          <label for="project-name" class="my-1">ชื่อโครงการ</label>
          <select class="custom-select my-1 mr-sm-2" name="project" id="project-name">
            <option value="<?php echo $prop_project ?>" selected><?php echo $prop_project ?></option>
            <option value="ไม่มี">ไม่มี</option>
            <option value="พลัสคอนโด (Plus Condo)">พลัสคอนโด (Plus Condo)</option>
            <option value="เดอะไรส์ เรสซิเดนท์ (The Rise Residence)">เดอะไรส์ เรสซิเดนท์ (The Rise Residence)</option>
          </select>
        </div>
        <div class="form-group col-md-2">
          <label for="building" class="my-1">อาคาร</label>
          <input type="text" name="building" id="building" class="form-control my-1" value="<?php echo $prop_building; ?>" >
        </div>
        <div class="form-group col-md-2">
          <label for="area" class="my-1">พื้นที่ตารางเมตร</label>
          <input type="text" name="area" id="area" class="form-control my-1" value="<?php echo $prop_space; ?>" >
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-3">
          <label for="bedroom" class="my-1">จำนวนห้องนอน</label>
          <input type="text" name="bedroom" id="bedroom" class="form-control my-1" value="<?php echo $prop_bedroom; ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="bathroom" class="my-1">จำนวนห้องน้ำ</label>
          <input type="text" name="bathroom" id="bathroom" class="form-control my-1" value="<?php echo $prop_bathroom; ?>" >
        </div>
        <div class="form-group col-md-3">
          <label for="floor" class="my-1">อยู่ชั้นที่</label>
          <input type="text" name="floor" id="floor" class="form-control my-1" value="<?php echo $prop_floor; ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="allfloor" class="my-1">จำนวนชั้นทั้งหมด</label>
          <input type="text" name="allfloor" id="allfloor" class="form-control my-1" value="<?php echo $prop_layernumber; ?>">
        </div>
      </div>

      <div class="form-row">
        <div class="form-group col-md-8">
          <label for="highlight">จุดเด่นของอสังหาฯ</label>
          <input type="text" name="highlight" id="highlight" class="form-control my-1" value="<?php echo $prop_highlight; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="view">วิว</label>
          <select class="custom-select my-1 mr-sm-2" name="view" id="view">
            <option value="<?php echo $prop_view ?>" selected><?php echo $prop_view ?></option>
            <option value="ไม่มี">ไม่มี</option>
            <option value="วิวเมือง">วิวเมือง</option>
            <option value="วิวธรรมชาติ">วิวธรรมชาติ</option>
          </select>
        </div>
      </div>
<!-- 
      <div class="form-group row">
        <label class="my-1 col-md-1" for="img1">รูปที่ 1 :</label>
        <div class="col-md-6">
          <input type="file" name="img1" id="img1" class="my-1" >
        </div>
        <div class="col-md-5">
          <img src="img/onpre.png" id="blah" width="100px" high="100px" >
        </div>
      </div>
      <div class="form-group row">
        <label class="my-1 col-md-1" for="img2">รูปที่ 2 :</label>
        <div class="col-md-6">
          <input type="file" name="img2" id="img2" class="my-1" >
        </div>
        <div class="col-md-5">
          <img src="img/onpre.png" id="blah2" width="100px" high="100px" >
        </div>
      </div>
      <div class="form-group row">
        <label class="my-1 col-md-1" for="img3">รูปที่ 3 :</label>
        <div class="col-md-6">
          <input type="file" name="img3" id="img3" class="my-1" >
        </div>
        <div class="col-md-5">
          <img src="img/onpre.png" id="blah3" width="100px" high="100px" >
        </div>
      </div>
      <div class="form-group row">
        <label class="my-1 col-md-1" for="img4">รูปที่ 4 :</label>
        <div class="col-md-6">
          <input type="file" name="img4" id="img4" class="my-1">
        </div>
        <div class="col-md-5">
          <img src="img/onpre.png" id="blah4" width="100px" high="100px" >
        </div>
      </div>
      <div class="form-group row">
        <label class="my-1 col-md-1" for="img5">รูปที่ 5 :</label>
        <div class="col-md-6">
          <input type="file" name="img5" id="img5" class="my-1" >
        </div>
        <div class="col-md-5">
          <img src="img/onpre.png" id="blah5" width="100px" high="100px" >
        </div>
      </div>
      <div class="form-group row">
        <label class="my-1 col-md-1" for="img6">รูปที่ 6 :</label>
        <div class="col-md-6">
          <input type="file" name="img6" id="img6" class="my-1" >
        </div>
        <div class="col-md-5">
          <img src="img/onpre.png" id="blah6" width="100px" high="100px" >
        </div>
      </div> -->

      <div class="row">
        <div class="col-md-4">
          <button type="submit" name="submit" class="btn btn-primary sarabun btn-block my-1">บันทึก</button>
        </div>
        <div class="col-md-4">
          <button type="reset" name="button" class="btn btn-danger sarabun btn-block my-1">ล้างข้อมูล</button>
        </div>
        <div class="col-md-4">
         <a href="../memberpost.php" class="btn btn-warning btn-block sarabun my-1" >ย้อนกลับ</a>
        </div>
      </div>

    </form>
  </div>
</main>

<!-- footer Zone -->

<!-- end footer zone -->

<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.min.js" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
        function readImg1(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readImg2(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah2').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readImg3(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah3').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readImg4(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah4').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readImg5(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah5').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readImg6(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah6').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#img1").change(function() {
            readImg1(this);
        });
        $("#img2").change(function() {
            readImg2(this);
        });
        $("#img3").change(function() {
            readImg3(this);
        });
        $("#img4").change(function() {
            readImg4(this);
        });
        $("#img5").change(function() {
            readImg5(this);
        });
        $("#img6").change(function() {
            readImg6(this);
        });
    </script>
</body>
</html>
<?php        

        if(isset($_POST['submit'])){     
      
          $operation = $_POST['operation'];
          $es_type = $_POST['type'];
          // $other = $_POST['other']; //----- add to new  type
          $topic = $_POST['topic'];
          $price = $_POST['price'];
          $direction = $_POST['direct'];
          $detail = $_POST['estate-detail'];
          $prov = $_POST['province'];
          $dist = $_POST['district'];
          $amp = $_POST['amphur'];
          $road = $_POST['road'];
          $soi = $_POST['soi'];
          $project = $_POST['project'];
          $build = $_POST['building'];
          $space = $_POST['area'];
          $bedroom = $_POST['bedroom'];
          $bathroom = $_POST['bathroom'];
          $floor = $_POST['floor'];
          $allfloor = $_POST['allfloor'];
          $highlight = $_POST['highlight'];
          $view = $_POST['view'];

          $update = "UPDATE properties SET prop_oper ='$operation',prop_type ='$es_type',prop_topic='$topic',prop_price='$price',prop_direct='$direction',prop_detail='$detail',
                                          prop_province='$prov',prop_amphur='$amp',prop_distric='$dist',prop_road='$road',prop_soi='$soi',prop_project='$project',prop_building='$build',
                                          prop_space='$space',prop_bedroom='$bedroom',prop_bathroom='$bathroom',prop_floor='$floor',prop_layernumber='$allfloor',prop_highlight='$highlight',prop_view='$view' where prop_id = '$id' ";
          if($conn->query($update)==true){
            echo '<script>alert("อัพเดทเรียบร้อย");</script>';  
            header("Refresh:0,url=../memberpost.php");
          }

          // date_default_timezone_set("Asia/Bangkok");
          // $now = new DateTime();
          // $time = ($now->format('dmY'));

          // $checkNumber = "SELECT * FROM proppost where post_date ='$time' AND post_member='$cus_id' ";
          // $resChkNum = $conn->query($checkNumber);
          // if($resChkNum->num_rows<=5){
          //   $checkRepeat = "SELECT * FROM properties inner join proppost on proppost.post_prop = properties.prop_id where prop_space = '$space' AND prop_price = '$price' AND post_member='$cus_id'  ";
          //   $resRepeat = $conn->query($checkRepeat);
          //   if($resRepeat->num_rows > 0){
          //     echo "<script>alert('ท่านสร้างโพสต์ไปเเล้วกรุณาตรวจสอบข้อมูล');</script>";
          //     header("Refresh:0,url=profile.php");
          //   }else{
          //     $numberPost = $resChkNum->num_rows;
          //     if($_POST['other'] != null ){
          //       $other = $_POST['other'];
          //       $insOth = "INSERT INTO proptype(type_name)VALUES('$other')";
          //       $conn->query($insOth);
          //     }

          //     // $insProp = "INSERT INTO properties (prop_oper,prop_type,prop_topic,prop_price,prop_direct,prop_detail,prop_province,prop_amphur,prop_distric,prop_road,prop_soi,
          //     //                                     prop_project,prop_building,prop_space,prop_bedroom,prop_bathroom,prop_floor,prop_layernumber,prop_highlight,prop_view) 
          //     //                                     VALUES ('$operation','$es_type','$topic','$price','$direction','$detail','$prov','$amp','$dist','$road','$soi'
          //     //                                     ,'$project','$build','$space','$bedroom','$bathroom','$floor','$allfloor','$highlight','$view')";
          //     if($conn->query($insProp)==TRUE){
          //       $getLast = "SELECT * FROM properties ORDER BY prop_id DESC LIMIT 1";
          //       $resGet = $conn->query($getLast);
          //       $res = $resGet->fetch_assoc();
          //       $last = $res['prop_id'];
          //       $newPost = $numberPost+1;
          //       $insToPost = "INSERT INTO proppost (post_prop,post_member,post_number,post_date)VALUES('$last','$cus_id','$newPost','$time')";
          //       if($conn->query($insToPost) == TRUE){

          //         $input = "INSERT INTO propimage (img_prop,img_name) VALUES ";

          //         for($i=1;$i<=6;$i++){
          //           if($_FILES["img".$i] != null){             
        
          //             $file = $_FILES["img".$i];
          //             $fileName = $file['name'];
          //             $filTmpName = $file['tmp_name'];
          //             $fileSize = $file['size'];
          //             $fileError = $file['error'];
          //             $fileType = $file['name'];
          //             if( $fileError == 0 ){
          //               $fileExt = explode('.', $fileName);
          //               $fileActualExt = strtolower(end($fileExt));
          //               print_r($fileExt);
          //               $allowed = array("jpeg","jpg","png");
                    
          //               date_default_timezone_set("Asia/Bangkok");
          //               $now2 = new DateTime();
          //               $time2 = ($now2->format('dmY'));                        
                      

          //               if (in_array($fileActualExt, $allowed)) {
                     
          //                   if ($fileError === 0) {
          //                       if ($fileSize < 200000000) {
          //                           $fileNameNew = $time2 ."".generateRandomString()."." . $fileActualExt;
          //                           $fileDestination = 'upload/' . $fileNameNew;
          //                           move_uploaded_file($filTmpName, $fileDestination);                                
          //                           $input .= "('$getLast','$fileNameNew'),";        
          //                       } else {
          //                           echo '<script>alert("Image Too Big");</script>';  
          //                       }
          //                   } else {
          //                       echo '<script>alert("Upload Image Fail");</script>';  
          //                   }
        
          //               } else {
          //                   echo '<script>alert("jpeg,jpg,png Only");</script>';  
          //               }
          //             }
          //         }else{
          //           return;
          //         }
        
          //         }            
                  
          //       $scInput = mb_substr($input, 0, -1);
          //       if($conn->query($scInput)){
          //         $conn->close();
          //         echo '<script>alert("ลงประกาศขายสำเร็จ");</script>';  
          //         header("Refresh:0,url=profile.php");
          //       }

          //       }else{
          //         echo $conn->error;
          //       }

          //     }else{
          //       echo $conn->error;
          //     }
          //   }
          // }else{
          //   echo "<script>alert('ท่านสร้างโพสต์ครบตามจำนวนเงื่อนไขของวันนี้เเล้ว');</script>";
          //   header("Refresh:0,url=profile.php");
          // }        

          
        }

?>