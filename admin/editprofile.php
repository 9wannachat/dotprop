<?php
    include '../condb/condb.php';
    $cus_id = $_GET['id'];
    $data = "SELECT * FROM member where mem_id = '$cus_id'";
    $result = $conn->query($data);
    if($result->num_rows>0){
        while($res = $result->fetch_assoc()){
            $name = $res['mem_name'];
            $mail = $res['mem_mail'];
            $img = $res['mem_pic'];
            $tel = $res['mem_tel'];
        }
    }

    $ex_name = explode(" ",$name);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dotprop</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
   
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    DOTPROP
                </a>
            </div>

            <ul class="nav">
                <!-- <li>
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li> -->
                <li>
                    <a href="memberpost.php">
                        <i class="pe-7s-news-paper"></i>
                        <p>Post</p>
                    </a>
                </li>
                <li  class="active">
                    <a href="member.php">
                        <i class="pe-7s-users"></i>
                        <p>Member</p>
                    </a>
                    <ul>
                        <li class="active">
                            <a href="editprofile.php">
                                <i class="pe-7s-users"></i>
                                <p>Edit Data Member</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="news.php">
                        <i class="pe-7s-ribbon"></i>
                        <p>NEWS</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">                             
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li>
                           <a href="">
                               <p>Account</p>
                            </a>
                        </li> -->
           
                        <li>
                            <a href="logout.php">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="header">
                                <h4 class="title">Edit Data</h4>
                                <hr>
                            </div>
                            <div class="content">
                            <form action="editprocess.php" method="POST" enctype="multipart/form-data">
                                
                            <div class="row">
                                <div class="col-md-2">                                  
                                    <img src="../imgprofile/<?php echo $img; ?>" id="blah" width="200px" hieght="200px" >                                    
                                </div>
                                <div class="col-md-6">
                                    <div class=" d-flex justify-content-center">
                                        <input type="file" name="file" id="imgInp" class="form-control">
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="<?php echo  $cus_id; ?>">
                                <div class="form-row">
                                    <div class="col-md-5">
                                    <label for="name">ชื่อ</label>
                                        <input type="text" name="fname" id="name" class="form-control" value="<?php echo $ex_name[0]; ?>" >
                                    </div>
                                    <div class="col-md-5">
                                        <label for="lname">สกุล</label>
                                        <input type="text" name="lname" id="lname" class="form-control" value="<?php echo $ex_name[1]; ?>" >
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-5">
                                        <label for="mail">Email</label>
                                        <input type="text" class="form-control" id="mail"  name="mail" value="<?php echo $mail; ?>">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="tel">เบอร์โทรศัพท์</label>
                                        <input type="text" class="form-control" id="tel" name="tel" value="<?php echo $tel; ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end">
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="submit" name="submit" class="btn btn-block btn-info">บันทึก</button>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="member.php" class="btn btn-block btn-warning" >ย้อนกลับ</a>
                                    </div>
                                </div>
                            </div>

                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
            var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>

        <footer class="footer">

        </footer>

    </div>
</div>


</body>


    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/chartist.min.js"></script>
    <script src="assets/js/bootstrap-notify.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
	<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
	<script src="assets/js/demo.js"></script>


</html>
