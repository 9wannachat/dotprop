<?php
    include '../condb/condb.php';
    session_start();

    $getPost = "SELECT * FROM proppost where post_verify = 1";
    $resPost = $conn->query($getPost);
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dotprop</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <style>
      .fix_line{
      
        line-height:1.2em;
        height:3.6em;
        overflow:hidden;
        white-space: nowrap;
      
      }
    </style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    DOTPROP
                </a>
            </div>

            <ul class="nav">
                <!-- <li>
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li> -->
                <li>
                    <a href="#">
                        <i class="pe-7s-news-paper"></i>
                        <p>Post</p>
                    </a>
                    <li>
                        <a href="memberpost.php">
                            <p>Nonverify</p>
                        </a>
                    </li>
                    <li class="active">
                        <a href="postaccept.php">
                            <p>Verify</p>
                        </a>
                    </li>
                </li>
              
                <li>
                    <a href="member.php">
                        <i class="pe-7s-users"></i>
                        <p>Member</p>
                    </a>
                </li>
                <li>
                    <a href="news.php">
                        <i class="pe-7s-ribbon"></i>
                        <p>NEWS</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Verify Post</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">                             
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <!-- <li>
                           <a href="">
                               <p>Account</p>
                            </a>
                        </li> -->
           
                        <li>
                            <a href="logout.php">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

                <div class="row">
                <?php
                    while($resPostdb = $resPost->fetch_assoc()){                        
                        $prop_post = $resPostdb['post_prop'];
                        $getPostDetail = "SELECT * FROM properties  inner join province on province.PROVINCE_ID = properties.prop_province
                                                                    inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur 
                                                                    inner join proppost on properties.prop_id = proppost.post_prop
                                                                    inner join status on status.status_id = proppost.post_verify
                                                                    inner join operation on operation.op_id = properties.prop_oper
                                                                    inner join proptype on proptype.type_id = properties.prop_type  where prop_id = '$prop_post'
                                                                    -- inner join propimage on propimage.img_prop = properties.prop_id where prop_id = '$prop_post'";
                        $resPostDetail = $conn->query($getPostDetail);
                        while($postDetail = $resPostDetail->fetch_assoc()){
                ?> 
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><?php echo $postDetail['prop_topic']; ?> </h4> <strong class="text-danger">&nbsp;[<?php echo $postDetail['status_name']; ?>]</strong>
                                <span class="badge badge-secondary"><?php echo $postDetail['op_name']; ?></span>
                                        <span class="badge badge-secondary"><?php echo $postDetail['type_name']; ?></span>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h5>ราคา <?php echo $postDetail['prop_price']; ?> ฿</h5>
                                    </div>
                                    <div class="col-md-7">
                                        <h5 class="card-subtitle mb-2 sarabun my-3">
                                            <i class="pe-7s-map-marker"></i>&nbsp; <?php echo $postDetail['AMPHUR_NAME']; ?>,<?php echo $postDetail['PROVINCE_NAME']; ?>
                                        </h5> 
                                    </div>                                   
                                </div>
                               
                            </div>
                            <div class="content">  
                            <p class="card-text fix_line"><?php echo $postDetail['prop_detail']; ?></p>
                            <table class="table table-bordered text-center sarabun">
                            <tr>
                                <td><i class="fas fa-ruler-combined"></i> <?php echo $postDetail['prop_space']; ?> ตร.ม.</td>
                                <td><i class="fas fa-bed"></i> <?php echo $postDetail['prop_bedroom']; ?> ห้องนอน</td>
                                <td><i class="fas fa-bath"></i> <?php echo $postDetail['prop_bathroom']; ?> ห้องน้ำ</td>
                            </tr>
                            </table>                  
                                <div class="footer">
                                    <hr>
                                    <div class="d-flex justify-content-end">     
                                        <a href="edit/edit.php?id=<?php echo $postDetail['prop_id']; ?>" class="btn btn-warning">แก้ไข</a>  
                                        <button onclick="clicked()" class="btn btn-danger"> ลบ </button> 
                                        <a class="btn btn-success" href="verify.php?id=<?php echo $postDetail['prop_id']; ?>">ยืนยันประกาศ</a>
                                    </div>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        function clicked(){
                        var ck = confirm("ต้องการลบประกาศขายนี้จริงหรือ");
                            if(ck==true){
                            window.location.href = 'http://localhost/dotprop/admin/deletepost.php?id=<?php echo $postDetail['prop_id'] ; ?>';
                            }else{
                            return;
                            }
                        }
                    </script>   

                <?php
                        }
                    }
                ?>
                    
                </div>

               

            </div>
        </div>

        


        <footer class="footer">

        </footer>

    </div>
</div>
   

<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>
</body>



</html>
