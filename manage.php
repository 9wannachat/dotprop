<?php
    include 'condb/condb.php';
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $edit  = "SELECT * FROM proppost  inner join properties on properties.prop_id = proppost.post_prop 
                                          inner join operation on operation.op_id = properties.prop_oper
                                          inner join proptype on proptype.type_id = properties.prop_type
                                          inner join province on province.PROVINCE_ID = properties.prop_province
                                          inner join district on district.DISTRICT_ID = properties.prop_distric
                                          inner join amphur on amphur.AMPHUR_ID = properties.prop_amphur
                                          inner join propimage on propimage.img_prop = proppost.post_prop where post_prop = '$id' ";
        $result = $conn->query($edit);
       
        while($row = $result->fetch_assoc()){
        $prop_id = $row['prop_id'];
        $prop_oper = $row['prop_oper'];
        $prop_type = $row['prop_type'];
        $prop_topic = $row['prop_topic'];
        $prop_price = $row['prop_price'];
        $prop_direct = $row['prop_direct'];
        $prop_detail = $row['prop_detail'];
        $prop_province = $row['PROVINCE_NAME'];
        $prop_amphur = $row['AMPHUR_NAME'];
        $prop_distric = $row['DISTRICT_NAME'];
        $prop_road = $row['prop_road'];
        $prop_soi = $row['prop_soi'];
        $prop_project = $row['prop_project'];
        $prop_building = $row['prop_building'];
        $prop_space = $row['prop_space'];
        $prop_bedroom = $row['prop_bedroom'];
        $prop_bathroom = $row['prop_bathroom'];
        $prop_floor= $row['prop_floor'];
        $prop_layernumber = $row['prop_layernumber'];
        $prop_highlight = $row['prop_highlight'];
        $prop_view = $row['prop_view'];
        $oper = $row['op_name'];
        $type = $row['type_name'];
       
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dotprop</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/dotprop.css">
    <link rel="stylesheet" href="css/all.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="css/megamenu.css">
    <link rel="stylesheet" href="css/modalsb.css">
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Sarabun" rel="stylesheet">
</head>
<body>

    <div class="container">
        
        <div class="card">
            <div id ="print">
            <div class="card-header">
                <div class="d-flex justify-content-center">
                    <h4><?php echo $prop_topic;  ?></h4>
                </div>
            </div><br>

            <div class="d-flex justify-content-center">                
                <?php
                    $getImg ="SELECT * FROM propimage WHERE img_prop = '$id'";
                    $resImg = $conn->query($getImg);
                    while($res = $resImg->fetch_assoc()){
                       ?>
                       <div class="row">
                        <div class="col-auto">
                                <img class="" src="upload/<?php echo $res['img_name'];  ?> " class="img-fluid" width="400px" >
                        </div> 
                       </div>                                             
                       <?php
                    }
                ?>
            </div><br>
            <div class="d-flex justify-content-center"> 
                <div class="card">
                    <div class="card-body">
                        <b>คำอธิบาย</b>
                        <p><?php echo $prop_detail; ?></p>
                        <b><?php echo $type." ".$oper; ?></b>
                        <p>ราคา : <?php echo $prop_price; ?> บาท</p>
                        <p> <strong>ที่ตั้ง</strong> ตำบล : <?php echo $prop_distric;?> อำเภอ : <?php echo $prop_amphur;?> จังหวัด : <?php echo $prop_province;?> ถนน : <?php echo $prop_road;?> ซอย : <?php echo $prop_soi;?></p>
                       
                        <b>รายละเอียด</b><br>
                        <p><strong>ชื่อโครงการ</strong> <?php echo $prop_project;?></p>
                        <p><strong>พื้นที่</strong> <?php echo $prop_space;?> ตารางเมตร</p>
                        <p><strong>ห้องนอน</strong> <?php echo $prop_bedroom;?> ห้อง <strong>ห้องน้ำ</strong> <?php echo $prop_bathroom;?> ห้อง </p>
                        <p><strong>จุดเด่น</strong> <?php echo $prop_highlight;?></p>
                        <p><strong>วิว</strong> <?php echo $prop_view;?></p>                       
                       
                        </div>                       
                    </div>
                </div>
            </div>
           
        </div> <br>
                    <div class="row">
                            <div class="col-md-3">
                                <a href="profile.php" class="btn btn-block btn-warning">ย้อนกลับ</a>  
                            </div>
                            <div class="col-md-3">
                                <button onclick="clicked()" class="btn btn-block btn-danger">ลบ</button>
                            </div>
                            <div class="col-md-3">
                                <a href="editpost.php?id=<?php echo  $prop_id ; ?>" class="btn btn-block btn-success">แก้ไข</a>
                            </div>
                            <div class="col-md-3">
                                <button onclick="onprint()" class="btn btn-block btn-info">พิมพ์ PDF</button>                              
                            </div>
                            
                        </div>
    </div>

    <script>
        function clicked(){
              var ck = confirm("ต้องการลบประกาศขายนี้จริงหรือ");
                if(ck==true){
                  window.location.href = 'http://localhost/dotprop/deletepost.php?id=<?php echo $prop_id ; ?>';
                }else{
                  return;
                }
              }

       function onprint (){
            var divContents = $("#print").html();
            var printWindow = window.open('', '', 'height=800,width=1000,headers=no,footer=no');
            printWindow.document.write('<html><head><title></title>');
            printWindow.document.write('<link rel="stylesheet" href="style.css">');
            printWindow.document.write('<link rel="stylesheet" href="css/bootstrap.min.css">');
            printWindow.document.write('<style>.position (width: 50rem;   margin-top:10rem;)</style>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
       };
    </script>


<script src="js/jquery-3.3.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/holder.min.js" charset="utf-8"></script>
</body>
</html>